package mappingbotapp.model;

import org.junit.Test;

/*
* Tests for the PacketDecoder class.
* Test are optional - to run tests, install JUnit.
*/
public class PacketDecoderTest
{
    
    public PacketDecoderTest()
    {
    }

    /*
     * Test decoding with no data.
     */
    @Test(expected = NullPointerException.class)
    public void testDecodeNullData()
    {
        byte[] rawData = null;
        BotPacket result = PacketDecoder.decode(rawData);
    }
    
    /*
    * Test decoding with incorrect data length.
    */
    @Test(expected = IllegalArgumentException.class)
    public void testDecodeIncorrectLength()
    {
        byte[] rawData = {0};
        BotPacket result = PacketDecoder.decode(rawData);
    }
    
    /*
    * Test decoding with random data. Check that the decoded results are equal
    * to the input.
    */
    @Test
    public void testDecodeRandomData()
    {
        /*byte statusByte = 0;
        byte[] depthBytes = {0, 0};
        byte[] latLonBytes = {longToBytes(0), longToBytes(0)};
        byte[] velocityBytes = {0};
        byte[] rotationBytes = {0};
        
        byte[] rawData = */
    }
}
