package mappingbotapp.model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import javafx.embed.swing.JFXPanel;

import mappingbotapp.generic.AbstractCallback;
import mappingbotapp.generic.Vector3;

/*
* Tests for the DataStoreModel class.
* Test are optional - to run tests, install JUnit.
*/
public class DataStoreModelTest
{
    enum EventHandlerResult
    {
        UNDEFINED,
        SUCCESS,
        FAILURE
        
    }
    // Store the result of the last callback method
    private EventHandlerResult lastEventResult = EventHandlerResult.UNDEFINED;
    
    public DataStoreModelTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
        // This initialzes the JavaFX toolkit
        JFXPanel fxPanel = new JFXPanel();
    }

    /**
     * Test of export method, of class DataStoreModel.
     */
    @Test
    public void testExport()
    {
        resetState();
        
        // Create a temporary file to test the export
        File saveFile = new File("exportTestFile.csv");
        if (!saveFile.exists()) {
            try
            {
                saveFile.createNewFile();
            }
            catch (IOException e)
            {
                fail("Failed creating export file.");
            }
        }
        
        // Initialize data store
        DataStoreModel dataStore = new DataStoreModel();
        // Add some fake data
        for (int i = 0; i < 100; i++)
        {
            dataStore.add(new Vector3(i, i*10, i*Math.PI));
        }
        
        dataStore.export(saveFile, new GenericSuccessHandler(), new GenericFailureHandler());
        
        // Leave time for the callable to execute
        sleep();
        
        // Ensure the export was deemed a success
        assertEquals(EventHandlerResult.SUCCESS, lastEventResult);
        
        // Clear the data store (also test the reset() method)
        dataStore.reset();
        assertEquals(dataStore.size(), 0);
        
        // Test that export was actually successful by importing the data and checking it
        dataStore.importFromFile(saveFile, new GenericSuccessHandler(), new GenericFailureHandler());
        sleep();
        assertEquals(EventHandlerResult.SUCCESS, lastEventResult);
        for (int i = 0; i < 100; i++)
        {
            assertEquals(dataStore.get(i), new Vector3(i, i*10, i*Math.PI));
        }
    }
    
    /**
     * Test of importFromFile method, of class DataStoreModel.
     */
    @Test
    public void testImportFromFile()
    {
        resetState();
        
        // Importing of fake data is implicitly tested in the export test
        // So in this test, just check that incorrectly formatted data is identified
        File saveFile = new File("importTestFile.csv");
        try
        {
            FileWriter writer = new FileWriter(saveFile);

            // Write data that should be identified as incorrectly formatted
            writer.append("X [m],Y [m],Depth [m]");
            writer.append('\n');
            writer.append("abcdef");
            writer.flush();
            writer.close();
            
            DataStoreModel dataStore = new DataStoreModel();
            dataStore.importFromFile(saveFile, new GenericSuccessHandler(), new GenericFailureHandler());
            
            sleep();
            assertEquals(EventHandlerResult.FAILURE, lastEventResult);
        }
        catch (IOException e)
        {
            fail("Error creating import file.");
        }
    }
    
    private void sleep()
    {
        try
        {
            TimeUnit.SECONDS.sleep(1);
        }
        catch (InterruptedException e)
        {
            fail("Interrupted exception thrown.");
        }
    }
        
    private void resetState()
    {
        lastEventResult = EventHandlerResult.UNDEFINED;
    }

    class GenericSuccessHandler extends AbstractCallback<Void>
    {
        @Override
        public void call(Void v)
        {
            lastEventResult = EventHandlerResult.SUCCESS;
        }
    }
    
    class GenericFailureHandler extends AbstractCallback<Void>
    {
        @Override
        public void call(Void v)
        {
            lastEventResult = EventHandlerResult.FAILURE;
        }
    }
}
