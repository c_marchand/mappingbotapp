package io.github.jdiemke.triangulation;

/**
 * 2D edge class implementation.
 * 
 * @author Johannes Diemke
 */
public class Edge3D {

    public Vector3D a;
    public Vector3D b;

    /**
     * Constructor of the 2D edge class used to create a new edge instance from
     * two 2D vectors describing the edge's vertices.
     * 
     * @param a
     *            The first vertex of the edge
     * @param b
     *            The second vertex of the edge
     */
    public Edge3D(Vector3D a, Vector3D b) {
        this.a = a;
        this.b = b;
    }

}