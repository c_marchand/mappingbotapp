package mappingbotapp.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;

// XBee related imports
import com.digi.xbee.api.RemoteXBeeDevice;
import com.digi.xbee.api.XBeeDevice;
import com.digi.xbee.api.exceptions.XBeeException;
import com.digi.xbee.api.models.XBee64BitAddress;

import gnu.io.SerialPort;
import gnu.io.CommPortIdentifier;

import mappingbotapp.generic.AbstractCallback;

// Ensure to link RXTX with proper library for the
// system OS. (for windows, right click project, go to "Run" section, add VM parameter:
// -Djava.library.path=libs/extra-libs/native/Windows/win64

/*
* This class implements the communication between this application and the bot.
*/
public class CommunicationModel implements ICommunicationModel, Runnable
{
    // Xbee parameters
    private static final int BAUD_RATE = 115200;
    private static final int DATA_BITS = SerialPort.DATABITS_8;
    private static final int STOP_BITS = SerialPort.STOPBITS_1;
    private static final int PARITY = SerialPort.PARITY_NONE;
    private static final int FLOW_CONTROL = SerialPort.FLOWCONTROL_NONE;
    
    private static final int COM_PORT_REFRESH_INTERVAL = 1000; // in ms
    
    private final IControlModel controlModel;
    private final BotDataReceiveListener dataReceiveListener;
    
    private XBeeDevice localDevice = null;

    // Descriptive list of COM ports available
    private ObservableList<ComPort> availableComPorts = FXCollections.observableArrayList();
    
    private byte[] lastMotorCommand;
    
    public CommunicationModel(IControlModel controlModel)
    {
        this.controlModel = controlModel;
        
        dataReceiveListener = new BotDataReceiveListener(this, controlModel);
        
        listPorts();
 
        // Add a thread that listens for any COM port updates
        Thread comPortUpdateThread = new Thread(this);
        comPortUpdateThread.start();
    }

    public void resendLastCommand()
    {
        sendBytes(lastMotorCommand);
    }
    
    @Override
    public void run()
    {
        // This thread will continually update the COM ports that are connected
        while (true)
        {
            try
            {
                Platform.runLater(new Runnable()
                {
                    public void run()
                    {
                        listPorts();
                    }
                });
                
                Thread.sleep(COM_PORT_REFRESH_INTERVAL);
            }
            catch (InterruptedException e)
            {
                // Do nothing
            }
        }
    }
    
    private void listPorts()
    {
        // Delete any ports that don't exist; add any that do exist
        // (Can't just clear and restart because that will deselect the currently
        // selected port)
        List<String> existingIds = new ArrayList<String>();
                
        java.util.Enumeration<CommPortIdentifier> portEnum = CommPortIdentifier.getPortIdentifiers();
        while ( portEnum.hasMoreElements() ) 
        {
            CommPortIdentifier portIdentifier = portEnum.nextElement();
            
            if (portIdentifier.getPortType() == CommPortIdentifier.PORT_SERIAL)
            {
                existingIds.add(portIdentifier.getName());
                ComPort newPort = new ComPort(portIdentifier.getName(), getPortTypeName(portIdentifier.getPortType()));
                if (!availableComPorts.contains(newPort))
                    availableComPorts.add(newPort);
            }
        }

        List<ComPort> portsToRemove = new ArrayList<ComPort>();
        for (ComPort port : availableComPorts)
        {
            if (!existingIds.contains(port.id))
            {
                portsToRemove.add(port);
            }
        }
        
        for (ComPort port : portsToRemove)
        {
            availableComPorts.remove(port);
        }
    }
    
    private static String getPortTypeName (int portType)
    {
        switch (portType)
        {
            case CommPortIdentifier.PORT_I2C:
                return "I2C";
            case CommPortIdentifier.PORT_PARALLEL:
                return "Parallel";
            case CommPortIdentifier.PORT_RAW:
                return "Raw";
            case CommPortIdentifier.PORT_RS485:
                return "RS485";
            case CommPortIdentifier.PORT_SERIAL:
                return "Serial";
            default:
                return "unknown type";
        }
    }
    
    @Override
    public ObservableList<ComPort> getAvailableComPorts()
    {
        return availableComPorts;
    }
 
    /*
    * ICommunicationModel methods
    * Callables are used because we may want not want to block the UI.
    */
    @Override
    public void sendConnectCommand(ComPort comPort,
            AbstractCallback successCallback, AbstractCallback failureCallback)
    {
        // Create a runnable to not block the UI thread
        // We could make things a little nicer using FutureTask
        Runnable runnable = () -> {     
            localDevice = new XBeeDevice(
                    comPort.id, 
                    BAUD_RATE, 
                    DATA_BITS, 
                    STOP_BITS, 
                    PARITY, 
                    FLOW_CONTROL
            );

            try
            {
                // Attempt to open local device
                localDevice.open();

                // Add listener for XBee messages
                localDevice.addDataListener(dataReceiveListener);            

                successCallback.callLater(null);
            }
            catch (XBeeException e)
            {
                failureCallback.callLater(e.toString());
                localDevice = null;
            }
        };
        
        Thread th = new Thread(runnable);
        th.start();
    }
    
    @Override
    public void sendDisconnectCommand(AbstractCallback successCallback, 
           AbstractCallback failureCallback)
    {
        // Tell the control model that we are disconnecting
        controlModel.handleDisconnect();

        // Stop listening & close connection
        if (localDevice != null)
        {
            localDevice.removeDataListener(dataReceiveListener);
            localDevice.close();
        }
        else
        {
            // Do nothing
        }
        
        successCallback.call(null);
    }
    
    // Send some bytes to the remote Xbee device
    private void sendBytes(byte[] data)
    {
        lastMotorCommand = data;
        RemoteXBeeDevice myRemoteXBeeDevice = new RemoteXBeeDevice(localDevice,
				    new XBee64BitAddress("0013A20041491EBD"));
        if (localDevice != null)
        {
            Runnable runnable = () -> {  
                try
                {
                    localDevice.setReceiveTimeout(200);
                    localDevice.sendData(myRemoteXBeeDevice, data);
                }
                catch (Exception e)
                {
                    // Do nothing
                }
            };
                    
            Thread th = new Thread(runnable);
            th.start();
        }
    }
    
    // Send command to motors
    @Override
    public void sendMotorCommand(MotorCommand leftMotorCommand, MotorCommand rightMotorCommand)
    {
        byte[] byteArr = new byte[3];
        
        // Flip right motor direction because propeller is installed in opposite
        // direction
        if (rightMotorCommand.direction == MotorCommand.Direction.FORWARD)
            rightMotorCommand.direction = MotorCommand.Direction.REVERSE;
        else
            rightMotorCommand.direction = MotorCommand.Direction.FORWARD;
        
        // First byte is device address (not important)
        byteArr[0] = 0x00;
        // Second byte is left motor power
        byteArr[1] = leftMotorCommand.toByte();
        // Third byte is right motor power
        byteArr[2] = rightMotorCommand.toByte();
        
        // Send the motor commands bytes over Xbee
        sendBytes(byteArr);
    }
}
