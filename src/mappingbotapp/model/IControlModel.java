package mappingbotapp.model;

import mappingbotapp.generic.Vector2;
import mappingbotapp.generic.Vector3;
import mappingbotapp.generic.AbstractCallback;

/*
* Interface for the Control model. The Control model performs the autonomous
* navigation function. The interface contains the Control model's public methods,
* which other parts of the software will need to use.
*/
public interface IControlModel
{
    // Set data store so that control model can add corrected coordinates to it
    public void setDataStore(IDataStoreModel dataStore);
    // Set communication model so that control model can send commands to bot
    public void setCommunicationModel(ICommunicationModel commModel);
            
    public void stopMapping();
    
    public void handleStartMappingCommand(AbstractCallback<Void> successCallback, 
            AbstractCallback<Void> failureCallback, AbstractCallback<Void> finishedCallback);
    
    public void handleStopMappingCommand(AbstractCallback<Void> successCallback, 
            AbstractCallback<Void> failureCallback);
    
    public boolean isMapping();
     
    // Handle disconnection from the device
    public void handleDisconnect();
    
    // deprecated - use handleBotLocationData instead
    public void handleNewCoordinate(Vector3 coord);
    
    public int handlePathGeneration(int N, int M, double dx, double dy, Vector2 startPos, double phi);
    
    public int getIndex();
    
    public void returnToOrigin();
    
    public void handleCalibration(BotLocationData message);
    
    // Receive message with all bot location data
    public void handleBotLocationData(BotLocationData message);
    
    public void processMoveCommand(MoveCommandType command);
    
    public void reset();
}
