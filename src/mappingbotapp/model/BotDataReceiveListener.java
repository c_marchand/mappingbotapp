package mappingbotapp.model;

import com.digi.xbee.api.listeners.IDataReceiveListener;
import com.digi.xbee.api.models.XBeeMessage;

/*
* This class receives XBee data and forwards it on.
*/
public class BotDataReceiveListener implements IDataReceiveListener
{
    private ICommunicationModel communicationModel;
    private IControlModel controlModel;
    
    public BotDataReceiveListener(ICommunicationModel commModel, IControlModel controlModel_)
    {
        communicationModel = commModel;
        controlModel = controlModel_;
    }
    
    /*
    * Data reception callback.
    */
    @Override
    public void dataReceived(XBeeMessage xbeeMessage)
    {	
        String address = xbeeMessage.getDevice().get64BitAddress().toString();
        String dataString = xbeeMessage.getDataString();
        
        // Check if first byte is not a resend request
        if ((xbeeMessage.getData()[0] & 0xff) != 0xff)
        {
            // Create a packet from the received data
            try
            {
                BotPacket packet = PacketDecoder.decode(xbeeMessage.getData());
                // Send the data to the control model
                controlModel.handleBotLocationData(packet.getLocationData());
            }
            catch (IllegalArgumentException e)
            {
                // Invalid packet - discard it
            }
        }
        else
        {
            // If a resend request is received, resend the last command
            communicationModel.resendLastCommand();
        }
    }
}
