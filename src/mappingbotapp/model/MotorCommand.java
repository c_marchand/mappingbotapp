package mappingbotapp.model;

/*
* Represents a command to be sent to the motors with direction and speed.
*/
public class MotorCommand
{
    public enum Speed
    {
        OFF,
        LOW_SPEED,
        MED_SPEED,
        HIGH_SPEED,
        VERY_LOW_SPEED
    }
    
    public enum Direction
    {
        FORWARD,
        REVERSE
    }
    
    public MotorCommand(Speed speed_, Direction direction_)
    {
        speed = speed_;
        direction = direction_;
    }
    
    public Speed speed;
    public Direction direction;
    
    // Byte representation to be sent to bot
    // 0-126 range is reverse, with 0 being the heighest speed
    // 127-128 is no power (staying still) (this must be verified)
    // 129-255 is forward, with 255 being the highest speed
    public byte toByte()
    {
        // Determine the magnitude of the speed, from 0-127
        int speedMag = 0;
        switch (speed)
        {
            case OFF:
                speedMag = 0;
                break;
            case LOW_SPEED:
                speedMag = 42;
                break;
            case MED_SPEED:
                speedMag = 85;
                break;
            case HIGH_SPEED:
                speedMag = 125;
                break;
            case VERY_LOW_SPEED:
                speedMag = 5;
                break;
        }
        
        // Convert the speed to 0-255 range based on direction
        int power = 0;
        if (direction == Direction.FORWARD)
        {
            power = 128 + speedMag;
        }
        else
        {
            power = 127 - speedMag;
        }
        
        byte powerByte = (byte) power;
        return powerByte;
    }
    
    // For debugging
    @Override
    public String toString()
    {
        StringBuffer result = new StringBuffer();
        //for (byte b : bytes) {
            result.append(String.format("%02X ", toByte()));
            result.append(" "); // delimiter
        //}
        return speed.toString() + ", " + direction.toString() + " (value: " + result + ")";
    }
}
