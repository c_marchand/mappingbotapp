package mappingbotapp.model;

import java.io.File;

import javafx.collections.ObservableList;

import mappingbotapp.generic.Vector3;
import mappingbotapp.generic.AbstractCallback;
import mappingbotapp.generic.GpsCoordinate;

/*
* Interface for the Data Store model. The Data Store model will contain the list
* of corrected data points. The interface contains the Data Store's public methods
* that need to be used by other parts of the software.
*/
public interface IDataStoreModel extends ObservableList
{
    public void setListener(IDataStoreListener listener_);
    
    public void addCoordinate(Vector3 newCoord);
    
    public void export(File saveFile, AbstractCallback<Void> successHandler, AbstractCallback<Void> failureHandler);
    
    public void importFromFile(File importFile, AbstractCallback<Void> successHandler, AbstractCallback<Void> failureHandler);
    
    public ObservableList getEstimatedPositions();
    
    public void addEstimatedPosition(GpsCoordinate estimatedPos);
    
    // Reset (empty) the data store
    public void reset();
    
    // Get heading in radians
    public double getHeading();
    
    // Set heading in radians
    public void setHeading(double heading);
}
