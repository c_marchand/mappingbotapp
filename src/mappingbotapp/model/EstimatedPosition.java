package mappingbotapp.model;

import mappingbotapp.generic.Vector2;
import mappingbotapp.generic.GpsCoordinate;

public class EstimatedPosition
{
    private Vector2 velocity = new Vector2(0,0);
    private Vector2 position = new Vector2(0,0);
    
    private double heading = 0; // radians
    private double headingVelocity = 0;
    
    // Update the estimated position based on time elapsed in ms since last update
    // and the currently active command
    public void update(long timestep, MoveCommand activeCommand)
    {
        double seconds = timestep / 1000.0;
        
        // Ensure heading is in correct range of -Pi to Pi
        if (heading > Math.PI)
        {
            heading = -2*Math.PI + heading;
        }
        else if (heading < -Math.PI)
        {
            heading = 2*Math.PI + heading;
        }
        
        Vector2 velocityChange = new Vector2(0,0);
         
        // Update the heading based on time at current heading velocity
        heading += headingVelocity * seconds;
        // Update the heading velocity based on drag
        double headingVelocityDrag = -Math.signum(headingVelocity)*0.0133*Math.pow(headingVelocity, 2);
        headingVelocity += headingVelocityDrag;
        
        Vector2 localizedVelocity = new Vector2(velocity.x, velocity.y);
        localizedVelocity.theta = velocity.theta - heading;

        // Change velocity based on current motor command
        switch (activeCommand.getType())
        {
            case FORWARD:
            case REVERSE:
                {
                    // If forward or reverse, then velocity should move towards being
                    // parallel to the current orientation
                    boolean forward = (activeCommand.getType() == MoveCommandType.FORWARD);
                    double desiredSpeed = (forward == true ? MoveCommand.MAX_SPEED : - MoveCommand.MAX_SPEED);
                    double localVelocity = localizedVelocity.r;
                    if (heading < 0)
                        localVelocity = -localVelocity;
                    double deltaDesired = (desiredSpeed - localVelocity);
                    
                    headingVelocity = 0;
                    
                    // Velocity is changed in the Y direction in local orientation
                    velocityChange.y += deltaDesired * MoveCommand.ACCELERATION * seconds;
                    velocityChange.PolarFix();

                }
                break;
            case LEFT:
                {
                    // Accelerate until max radial speed is reached
                    if (headingVelocity > -MoveCommand.MAX_RADIAL_SPEED)
                    {
                        headingVelocity -= MoveCommand.RADIAL_ACCELERATION * seconds;
                        headingVelocity = Math.min(headingVelocity, MoveCommand.MAX_RADIAL_SPEED);
                    }
                }
                break;
            case RIGHT:
                {
                    // Accelerate until max radial speed is reached
                    if (headingVelocity < MoveCommand.MAX_RADIAL_SPEED)
                    {
                        headingVelocity += MoveCommand.RADIAL_ACCELERATION * seconds;
                        headingVelocity = Math.max(headingVelocity, -MoveCommand.MAX_RADIAL_SPEED);
                    }
                }
                break;
            case STOP:
                // Gradually decelerate to zero
                // The drag does all the work here
                {
                }
                break;
        }
        
        // Change the velocity based on above calculation
        velocity = localizedVelocity;
        velocityChange.theta = heading;
        velocityChange.CarteFix();
        velocity.x += velocityChange.x;
        velocity.y += velocityChange.y;
        velocity.PolarFix();
        velocity.theta = heading;
        velocity.CarteFix();
       
        // Update position based on new velocity
        position.x = position.x + (velocity.x * seconds);
        position.y = position.y + (velocity.y * seconds);
        
        // Update velocity based on drag
        Vector2 dragVelocityChange = new Vector2(-1.20*Math.signum(velocity.x)*Math.pow(velocity.x,2)*seconds,-1.20*Math.signum(velocity.y)*Math.pow(velocity.y,2)*seconds);
        velocity = Vector2.displacement(velocity, new Vector2(-dragVelocityChange.x, -dragVelocityChange.y));
    }
    
    public GpsCoordinate getVector()
    {
        return new GpsCoordinate(position.x, position.y);
    }
    
    public Vector2 getCoord()
    {
        return new Vector2(position.x, position.y);
    }
    
    public Vector2 getVelocity()
    {
        return velocity;
    }
    
    public double getHeading()
    {
        return heading;
    }
    
    public void reset()
    {
        position = new Vector2(0,0);
        heading = 0;
        headingVelocity = 0;
        velocity = new Vector2(0,0);
    }
    
    public void setHeading(double newHeading)
    {
        heading = newHeading;
    }
}
