package mappingbotapp.model;

import javafx.collections.ObservableList;

import mappingbotapp.generic.AbstractCallback;

/*
* Interface for the Communication model. The Communication model controls the
* communication to and from the bot. This interface contains the Communication 
* model's public methods, which other parts of the software will need to use.
*/
public interface ICommunicationModel
{
    // Send connection or disconnection command. successCallback handles successful
    // connection, failureCallback handles failure and accepts a String description
    // of failure.
    public void sendConnectCommand(ComPort comPort, AbstractCallback<Void> successCallback, 
            AbstractCallback<String> failureCallback);
    public void sendDisconnectCommand(AbstractCallback<Void> successCallback, 
            AbstractCallback<String> failureCallback);
    
    // Send motor command to the bot
    public void sendMotorCommand(MotorCommand leftMotorCommand, MotorCommand rightMotorCommand);
    
    // Returns a list of descriptive strings for each COM port available
    // on the computer
    public ObservableList<ComPort> getAvailableComPorts();
    
    
    public void resendLastCommand();
}
