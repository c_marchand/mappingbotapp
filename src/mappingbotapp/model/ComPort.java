package mappingbotapp.model;

/*
* Information about a COM port, including a descriptive string representation.
*/
public class ComPort
{
    public String id;
    public String description;
    
    public ComPort(String id, String description)
    {
        this.id = id;
        this.description = description;
    }

    @Override
    public String toString()
    {
        return id + " - " + description;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (!ComPort.class.isAssignableFrom(obj.getClass()))
        {
            return false;
        }
        ComPort otherPort = (ComPort) obj;
        if (!this.id.equals(otherPort.id))
        {
            return false;
        }
        
        return true;
    }
}
