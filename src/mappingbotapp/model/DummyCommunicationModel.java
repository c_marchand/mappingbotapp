package mappingbotapp.model;

import java.util.Random;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import mappingbotapp.generic.Vector3;
import mappingbotapp.generic.AbstractCallback;
import mappingbotapp.generic.Vector2;

/*
* This class implements the Communication model's public methods to provide mock
* data for testing and development purposes.
* The use of the Runnable interface allows this to run in a thread so that it can
* do (mock) communications separately from the flow of the rest of the application. 
*/
public class DummyCommunicationModel implements ICommunicationModel, Runnable
{
    // For mock coordinate generation
    private static final int GRID_SIZE = 6;
    private static final double STEP_SIZE = 2.0;
    
    private Thread thread;
    private IControlModel controlModel;
    
    // For mock coordinate generation
    private double x = 0;
    private double y = 0;
    
    public DummyCommunicationModel(IControlModel controlModel)
    {
        this.controlModel = controlModel;
    }
    
    public void start()
    {
        System.out.println("Creating thread");
        
        //if (thread == null)
        //{
            thread = new Thread(this);
            thread.start();
        //}
    }
    
    @Override
    public void run()
    {
        // The thread will continue to do fake comms until it is interrupted
        double x = 0;
        double y = - STEP_SIZE;
        double z = -4;
        
        int operator_sign = 1; // Change to -1 when going down instead of up
                
        while(!Thread.interrupted())
        {
            try
            {
                if (controlModel.isMapping())
                {
                    z = z + (Math.random() > 0.5 ? 1 : -1) * (Math.random() * STEP_SIZE * 0.5);
                    
                    // Follow the Boustrephedon path
                    if ((y >= GRID_SIZE && operator_sign == 1) || (y <= 0 && operator_sign == -1))
                    {
                        x = x + STEP_SIZE;
                        operator_sign *= -1;
                    }
                    else
                    {
                        y = y + (STEP_SIZE * operator_sign);
                    }

                    if (x >= GRID_SIZE)
                    {
                        // We have reached the end
                        Thread.currentThread().interrupt();
                    }
                    else
                    {
                        // Add some noise to the x&y
                        BotLocationData mockData = new BotLocationData(new Vector3(0,0,0), new Vector3(0,0,0), new Vector3(0,0,0), 
                            new Vector2(y + (Math.random() > 0.5 ? 1 : -1) * (Math.random() * STEP_SIZE * 0.12), x + (Math.random() > 0.5 ? 1 : -1) * (Math.random() * STEP_SIZE * 0.12) ), (float) z);
                        controlModel.handleBotLocationData(mockData);
                    }
                }
                Thread.sleep(10);
            }
                catch (InterruptedException e)
            {
                // Something wants us to stop, so allow the thread to exit
                Thread.currentThread().interrupt();
            }
        }
        System.out.println("Exiting thread.");
    }
    
    /*
    * ICommunicationModel methods
    * Callables are used because we may want not want to block the UI.
    */
    @Override
    public void sendConnectCommand(ComPort comPort, AbstractCallback successCallback,
            AbstractCallback failureCallback)
    {
        Random rand = new Random();
        
        // Uncomment to add possibility of failure
        // if (Math.abs(rand.nextInt() % 10) > 2)
        if (1 == 1)
        {
            successCallback.call(null);

            // Start the thread which will run in parallel with the main
            // program and receive/send communications
            start();
        }
        else
        {
            failureCallback.call("Connection failed.");
        }
    }
    
    @Override
    public void sendDisconnectCommand(AbstractCallback successCallback, AbstractCallback failureCallback)
    {
        Random rand = new Random();
        
        // Uncomment to add possibility of failure
        // if (Math.abs(rand.nextInt() % 10) > 2)
        if (1 == 1)
        {
            successCallback.call(null);

            // Tell the control model that we are disconnecting
            controlModel.handleDisconnect();

            // Stop the thread
            thread.interrupt();
        }
        else
        {
            failureCallback.call("Disconnecting failed.");
        }
    }
    
    @Override
    public ObservableList<ComPort> getAvailableComPorts()
    {
        ComPort dummyPort = new ComPort("COM1", "COM1");
        
        return FXCollections.observableArrayList(dummyPort);
    }
    
    @Override
    public void sendMotorCommand(MotorCommand leftMotorCommand, MotorCommand rightMotorCommand)
    {
        //System.out.println("Sending left motor command: " + leftMotorCommand.toString());
        //System.out.println("Sending right motor command: " + rightMotorCommand.toString());
        
        // Do nothing (dummy)
    }
    
    @Override
    public void resendLastCommand()
    {
        // Do nothing
    }
}
