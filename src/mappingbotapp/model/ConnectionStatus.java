package mappingbotapp.model;

public enum ConnectionStatus
{
    CONNECTED,
    NOT_CONNECTED,
    ATTEMPTING_CONNECTION,
    CONNECTION_FAILED
}
