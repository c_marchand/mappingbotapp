package mappingbotapp.model;

import mappingbotapp.generic.Vector3;
import mappingbotapp.generic.Vector2;

// Represents all info needed for the control system
public class BotLocationData
{
    public final Vector3 position;
    public final Vector3 velocity;
    public final Vector3 rotation;
    public final Vector2 gpsCoordinate;
    public final float depth;
    
    public BotLocationData(Vector3 position_, Vector3 velocity_, Vector3 rotation_, 
            Vector2 gpsCoordinate_, float depth_)
    {
    	// Fixes the backward orientation of the phone in the boat.
    	position = position_;
        velocity = velocity_;
        rotation = Vector3.attitudeFix(rotation_);
        gpsCoordinate = gpsCoordinate_;
        depth = depth_;
    }
}
