package mappingbotapp.model;

/*
* BotPacket wraps data transmitted from the bot.
*/
public class BotPacket
{
    private final StatusCode statusCode;
    private final BotLocationData locationData;
    private boolean validPacket = true;
    
    public BotPacket(StatusCode statusCode_, BotLocationData locationData_)
    {
        statusCode = statusCode_;
        locationData = locationData_;
    }
    
    public StatusCode getStatusCode()
    {
        return statusCode;
    }
    
    public BotLocationData getLocationData()
    {
        return locationData;
    }
    
    public boolean isValid()
    {
        return validPacket;
    }
    
    public void setValidity(boolean validity)
    {
        validPacket = validity;
    }
}
