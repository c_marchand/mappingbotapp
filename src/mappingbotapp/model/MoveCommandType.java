package mappingbotapp.model;

/*
* MoveCommand represents a move command for the boat (as opposed to its' motors)
*/
public enum MoveCommandType
{
    STOP,
    FORWARD,
    REVERSE,
    LEFT,
    RIGHT,
    MOVING_LEFT,
    MOVING_RIGHT
}
