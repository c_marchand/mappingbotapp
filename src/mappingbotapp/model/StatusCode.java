package mappingbotapp.model;

public enum StatusCode
{
    ERROR_NO_BATTERY,
    ERROR_UNKNOWN,
    LOW_BATTERY,
    MED_BATTERY,
    FULL_BATTERY
}
