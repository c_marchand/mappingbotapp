package mappingbotapp.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import mappingbotapp.generic.Vector3;
import mappingbotapp.generic.AbstractCallback;
import mappingbotapp.generic.GpsCoordinate;

/*
* This class implements the Data Store model's public methods to hold, export
* and import data.
* Extending SimpleListProperty allows this class to be based on ArrayList and
* implements the methods required to make this an ObservableList.
*/
public class DataStoreModel extends SimpleListProperty implements IDataStoreModel
{
    private IDataStoreListener listener;
    private ObservableList estimatedPositions = FXCollections.observableArrayList();
    
    private double heading = 0.0;
    
    public DataStoreModel()
    {
        super(FXCollections.observableArrayList());
    }

    @Override
    public void setListener(IDataStoreListener listener_)
    {
        listener = listener_;
    }
    
    @Override
    public void addCoordinate(Vector3 newCoord)
    {
        this.add(newCoord);
        if (listener != null)
        {
            listener.onDataChange();
        }
    }
    
    @Override
    public void export(File saveFile, AbstractCallback successHandler, 
            AbstractCallback failureHandler)
    {
        try
        {
            FileWriter writer = new FileWriter(saveFile);

            // Create the header line
            writer.append("X [m]");
            writer.append(',');
            writer.append("Y [m]");
            writer.append(',');
            writer.append("Depth [m]");

            writer.append('\n');

            // Print coords row by row
            forEach((coord) -> { 
                try
                {
                    writer.append(coord.toString());
                    writer.append('\n');
                }
                catch (IOException e)
                {
                    failureHandler.callLater(null);
                }
            });

            writer.flush();
            writer.close();

            // Notify that export was sucessful
            successHandler.callLater(null);
        }
        catch (IOException e)
        {
            failureHandler.callLater(null);
        }
    }
    
    @Override
    public void importFromFile(File importFile, AbstractCallback successHandler,
            AbstractCallback failureHandler)
    {
        this.clear();
        
        boolean headerLine = true;
        
        // Open the imported file
        try (BufferedReader br = Files.newBufferedReader(importFile.toPath()))
        {
            String line = br.readLine();

            while (line != null)
            {
                if (!headerLine)
                {
                    String[] coord_tokens = line.split(",");
                    
                    // Ensure the CSV has correct number of commas
                    if (coord_tokens.length != 3)
                        throw new Exception();

                    // Create data from the line
                    Vector3 coord = new Vector3(Double.parseDouble(coord_tokens[0]), 
                            Double.parseDouble(coord_tokens[1]), Double.parseDouble(coord_tokens[2]));

                    // Ensure creation was successful
                    if (coord == null)
                    {
                        throw new Exception("Incorrect data format");
                    }
                    
                    // Add the coordinate to the data store
                    addCoordinate(coord);
                }
                headerLine = false;

                // Go to next line
                line = br.readLine();
            }
            
            // Notify that export was sucessful
            successHandler.callLater(null);
            
        }
        catch (IOException ioe)
        {
            failureHandler.callLater(null);
        }
        catch (Exception e)
        {
            failureHandler.callLater(null);
        }
    }
    
    @Override
    public void reset()
    {
        this.clear();
        estimatedPositions.clear();
        if (listener != null)
        {
            listener.onDataChange();
        }
    }
    
    @Override
    public ObservableList getEstimatedPositions()
    {
        return estimatedPositions;
    }
    
    @Override
    public void addEstimatedPosition(GpsCoordinate estimatedPos)
    {
        estimatedPositions.add(estimatedPos);
    }
    
    @Override
    public double getHeading()
    {
        return heading;
    }
    
    @Override
    public void setHeading(double rad)
    {
        heading = rad;
    }
}
