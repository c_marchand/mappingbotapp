package mappingbotapp.model;

import javafx.application.Platform;

import mappingbotapp.generic.AbstractCallback;
import mappingbotapp.generic.Vector2;
import mappingbotapp.generic.CoordinateList;
import mappingbotapp.generic.GpsCoordinate;
import mappingbotapp.generic.Vector3;

/*
* This class implements the Control model's public methods to provide mock
* data for testing and development purposes.
*/
public class DummyControlModel implements IControlModel
{
    private IDataStoreModel dataStore;
    private ICommunicationModel communicationModel;
    private boolean isFirstData = true;
    private boolean isMapping = false;
    private int n = 0;
    
    public BotState state = new BotState();
    
    public CoordinateList Path = new CoordinateList();
    
    @Override
    public boolean isMapping()
    {
        return isMapping;
    }
    
    @Override
    public void handleDisconnect()
    {
        isMapping = false;
    }
    
    @Override
    public void setDataStore(IDataStoreModel dataStore_)
    {
        dataStore = dataStore_;
    }
    
    @Override
    public void setCommunicationModel(ICommunicationModel comms)
    {
        communicationModel = comms;
    }
    
    @Override
    public void stopMapping()
    {
        // Do nothing
    }
    
    @Override
    public void handleStartMappingCommand(AbstractCallback successCallback, 
            AbstractCallback failureCallback, AbstractCallback finishedCallback)
    {
        try
        {
            isMapping = true;
            
            successCallback.call(null);
        }
        catch (Exception e)
        {
        }
    }
    
    public void handleStopMappingCommand(AbstractCallback successCallback, 
            AbstractCallback failureCallback)
    {
        try
        {
            isMapping = false;
            successCallback.call(null);
        }
        catch (Exception e)
        {
        }
    }
    
    public void handleNewCoordinate(Vector3 coord)
    {
        // For the mock model, just pass the coord on to the data store
        if (dataStore != null)
        {
            // Need to run later because this can be in a non-UI thread
            Platform.runLater(new Runnable()
            {
                @Override
                public void run()
                {
                    dataStore.addCoordinate(coord);
                }
            });
        }
    }
    
    public void handleBotLocationData(BotLocationData data)
    {
    	if (!isFirstData)
    	{
    		state.UpdateReadings(data);
    		dataStore.addCoordinate(new Vector3(data.gpsCoordinate.x, data.gpsCoordinate.y, data.depth));
    	}
    	else 
    	{
    		state.InitialState(data);
    		dataStore.addCoordinate(state.k_Pos1.toCoord().depthPair(data.depth));
    		isFirstData = false;
    	}
        
        dataStore.getEstimatedPositions().add(new GpsCoordinate(data.gpsCoordinate.x, data.gpsCoordinate.y));
    }
    
    @Override
    public void returnToOrigin()
    {
        // Do nothing
    }
    
    @Override
    public void handleCalibration(BotLocationData data)
    {
    	// Do nothing
    }

    @Override
    public int handlePathGeneration(int N, int M, double dx, double dy, Vector2 startPos, double phi)
    {
    	
    	/* 
    	 * Generates a Boustrophedon path for the bot
    	 * of N by M points and with dx and dy spacing.
    	 * The hole set can be rotated about (0, 0)by phi
    	 * and then shifted to start at startPos.
    	 */
    	
    	for (int i = 0; i < N; i++)
    	{
    		for (int j = 0; j < M; j++)
    		{
    			if (i%2==0) {
    				Path.list.add(new Vector2(dx*i, dy*j));
    			}
    			else
    			{
    				Path.list.add(new Vector2(dx*i, dy*(M - 1 - j)));
    			}
    		}
    	}
    	Path.Rotate(phi);
    	Path.Shift(startPos);
    	return N*M;
    }
    
    public int getIndex()
    {
    	return n;
    }
    
    @Override
    public void processMoveCommand(MoveCommandType command)
    {
        // Do nothing
    }
    
    @Override
    public void reset()
    {
        // Do nothing
    }
}
