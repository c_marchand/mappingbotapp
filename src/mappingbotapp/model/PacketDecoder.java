package mappingbotapp.model;

import java.util.Date;

import mappingbotapp.generic.ByteUtil;
import mappingbotapp.generic.Vector2;
import mappingbotapp.generic.Vector3;

public class PacketDecoder
{
    // This is the size the packet should be - if the packet is a different size,
    // it will be discarded
    private static final int PACKET_SIZE = 61;
    
    // Decode raw bytes received into BotPacket
    public static BotPacket decode(byte[] rawData) throws IllegalArgumentException
    {
        // Ensure packet is of correct size
        if (rawData.length != PACKET_SIZE)
        {
            throw new IllegalArgumentException();
        }
        
        // Decode the status code
        StatusCode statusCode;
        byte statusByte = rawData[0];
        if (ByteUtil.isBitSet(statusByte, 1) && ByteUtil.isBitSet(statusByte, 0))
        {
            statusCode = StatusCode.FULL_BATTERY;
        }
        else if (ByteUtil.isBitSet(statusByte, 1) && !ByteUtil.isBitSet(statusByte, 0))
        {
            statusCode = StatusCode.MED_BATTERY;
        }
        else if (ByteUtil.isBitSet(statusByte, 0))
        {
            statusCode = StatusCode.LOW_BATTERY;
        }
        else if (statusByte == 0)
        {
            statusCode = StatusCode.ERROR_NO_BATTERY;
        }
        else
        {
            statusCode = StatusCode.ERROR_UNKNOWN;
        }
        
        // Decode the depth
        // If there is an issue here, may have to swap byte order
        byte[] depthBytes = { rawData[1], rawData[2] };
        // Depth is not an int (two bytes)
        double k=1.91e-4;
        double depth = k*((depthBytes[1] & 0xFF) + ((depthBytes[0] & 0xFF) * 256));
        
        Vector3 rotation = new Vector3(0,0,0);
        Vector3 position = new Vector3(0,0,0);
        Vector3 velocity = new Vector3(0,0,0);
        Vector2 gpsCoordinate = new Vector2(0,0);
        Date timestamp;
        
        // Get longitude
        int byteIndex = 2;
        byte[] lonBytes = {rawData[byteIndex+1], rawData[byteIndex+2], rawData[byteIndex+3], rawData[byteIndex+4], rawData[byteIndex+5], rawData[byteIndex+6], rawData[byteIndex+7], rawData[byteIndex+8]};
        gpsCoordinate.x = ByteUtil.bytesToDouble(lonBytes);
        byteIndex += 8;
        
        // Get latitude
        byte[] latBytes = {rawData[byteIndex+1], rawData[byteIndex+2], rawData[byteIndex+3], rawData[byteIndex+4], rawData[byteIndex+5], rawData[byteIndex+6], rawData[byteIndex+7], rawData[byteIndex+8]};
        gpsCoordinate.y = ByteUtil.bytesToDouble(latBytes);
        byteIndex += 8;
        
        // Get velocity
        byte[] vxBytes = {rawData[byteIndex+1], rawData[byteIndex+2], rawData[byteIndex+3], rawData[byteIndex+4]};
        velocity.x = ByteUtil.bytesToFloat(vxBytes);
        byteIndex += 4;
        
        byte[] vyBytes = {rawData[byteIndex+1], rawData[byteIndex+2], rawData[byteIndex+3], rawData[byteIndex+4]};
        velocity.y = ByteUtil.bytesToFloat(vyBytes);
        byteIndex += 4;
        
        byte[] vzBytes = {rawData[byteIndex+1], rawData[byteIndex+2], rawData[byteIndex+3], rawData[byteIndex+4]};
        velocity.z = ByteUtil.bytesToFloat(vzBytes);
        byteIndex += 4;
        
        // Get rotation
        byte[] rxBytes = {rawData[byteIndex+1], rawData[byteIndex+2], rawData[byteIndex+3], rawData[byteIndex+4]};
        rotation.x = ByteUtil.bytesToFloat(rxBytes);
        byteIndex += 4;
        
        byte[] ryBytes = {rawData[byteIndex+1], rawData[byteIndex+2], rawData[byteIndex+3], rawData[byteIndex+4]};
        rotation.y = ByteUtil.bytesToFloat(ryBytes);
        byteIndex += 4;
        
        byte[] rzBytes = {rawData[byteIndex+1], rawData[byteIndex+2], rawData[byteIndex+3], rawData[byteIndex+4]};
        rotation.z = ByteUtil.bytesToFloat(rzBytes);
        byteIndex += 4;
        
        // Get previous timestamp
        byte[] prevTime = {rawData[byteIndex+1], rawData[byteIndex+2], rawData[byteIndex+3], rawData[byteIndex+4], rawData[byteIndex+5], rawData[byteIndex+6], rawData[byteIndex+7], rawData[byteIndex+8]};
        long time = ByteUtil.bytesToLong(prevTime);
        byteIndex += 8;
        
        Date prevTimestamp = new java.util.Date(time/1000000);
        
        // Get current timestamp
        byte[] curTime = {rawData[byteIndex+1], rawData[byteIndex+2], rawData[byteIndex+3], rawData[byteIndex+4], rawData[byteIndex+5], rawData[byteIndex+6], rawData[byteIndex+7], rawData[byteIndex+8]};
        long curTimeLong = ByteUtil.bytesToLong(curTime);
        byteIndex += 8;
        
        timestamp = new java.util.Date(curTimeLong/1000000);
        
        // Adjust depth for conversion factor from water to air
        depth = -depth * 4.01;
        
        // Create the output packet
        BotLocationData locationData = new BotLocationData(new Vector3(0,0,0), new Vector3(0,0,0), rotation, 
            gpsCoordinate, (float) depth);
        BotPacket packet = new BotPacket(statusCode, locationData);
        
        if (rawData.length != PACKET_SIZE)
        {
            packet.setValidity(false);
            System.out.println("\nDiscarded invalid packet (length: " + rawData.length);
        }
        
        return packet;
    }
}
