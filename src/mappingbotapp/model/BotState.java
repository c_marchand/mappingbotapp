package mappingbotapp.model;

import mappingbotapp.generic.Vector3;
import mappingbotapp.generic.GpsCoordinate;
import mappingbotapp.generic.Vector2;
import mappingbotapp.generic.Matrix;

public class BotState 
{

	// Variables needed for state updates
	protected double curr_Time = 0;                    
	protected Vector3 b_Acc = new Vector3(0,0,0);
	protected Vector3 attitude = new Vector3(0,0,0);
	public Vector2 GPS_Pos = new Vector2(0,0);
	
	/*
	 *  I'm leaving this in case we change to sending the
	 *  Rotation Vector from the phone.
	 */
	
//	private Quaternion rotVec = new Quaternion(0,0,0);
	
	// Variables calculated during updates
	private Vector3 last_Acc = new Vector3(0,0,0);
	private Vector3 last_Vel = new Vector3(0,0,0);
	private Vector3 last_Dis = new Vector3(0,0,0);
	public Vector2 last_Pos = new Vector2(0,0);
	protected double last_Time = 0;
	
	protected Vector3 l_Vel = new Vector3(0,0,0);
	protected Vector3 k_Pos1 = new Vector3(0,0,0);
	protected Vector3 k_Pos2 = new Vector3(0,0,0);
	protected Vector3 l_Acc = new Vector3(0,0,0);
	
	// Variables set only once
	protected Vector3 k_Shift = new Vector3(0,0,0);  // Likely not needed
	protected double altitude = 0;
	
	// These are some WGS-84 constants.
	
	// Semi-major (equitorial) radius of the earth
	private static double aa = 6378137;
	// Semi-minor (polar) radius of the earth
	private static double bb = 6356752.3142;
	// Eccentricity
	private static double ee = 0.08181919;
	
	public void InitialState(BotLocationData Data) 
	{
		
//		altitude = Data.altitude;
//		
//		//last_Time = Data.timestamp.getTime();
//		
//		//last_Acc = Data.acceleration;
//                last_Acc = new Vector3(0,0,0);
//		
//		last_Pos = Data.gpsCoordinate;
//		
//		Vector3 e_Pos = WGS2ECEF(last_Pos,altitude);
//		
//		Matrix Rel = ECEFRotMatrix(last_Pos);
//		
//		Rel.transpose();
//		k_Shift = Matrix.VecMult(Rel,e_Pos);
//		k_Shift.x = -k_Shift.x;
//		k_Shift.y = -k_Shift.y;
//		k_Shift.z = -k_Shift.z;
//		
//		k_Shift.rFix();
//		
	}
	
	public void UpdateReadings(BotLocationData Data) 
	{
//		/* Use this to update measurements */
//		
//		//curr_Time = Data.timestamp.getTime();
//                
//		
//		//b_Acc = Data.acceleration;
//                b_Acc = new Vector3(0,0,0);
//		
//		attitude = Data.rotation;
//		
//		GPS_Pos = Data.gpsCoordinate;
//		
//		Vector3 b_Vel = Integrate3Axis(b_Acc,last_Acc,last_Vel,curr_Time,last_Time);
//		Vector3 b_Dis = Integrate3Axis(b_Vel,last_Vel,last_Dis,curr_Time,last_Time);
//		
//		Vector3 b_Vel = Integrate3Axis(b_Acc,last_Acc,last_Vel,Data.timestamp/1000000000,Data.prevTimestamp/1000000000);
//		Vector3 b_Dis = Integrate3Axis(b_Vel,last_Vel,last_Dis,1.0,0.5);
//		
//		Matrix Rlb = Attitude2RotMat(attitude);
//		
//		//System.out.println(Rlb);
//		
//		Matrix Rel = ECEFRotMatrix(GPS_Pos);
//		
//		l_Vel = Matrix.VecMult(Rlb,b_Vel);
//		l_Acc = Matrix.VecMult(Rlb,b_Acc);
//		
//		Vector3 l_Dis = Matrix.VecMult(Rlb,b_Dis);
//		
//		k_Pos1 = l_Dis; // Updates the current position in the 
//		                // computational (k) frame.
//		
//		Vector3 e_Dis = Matrix.VecMult(Rel,l_Dis);
//		
//		Vector3 last_e_Pos = WGS2ECEF(last_Pos,altitude);
//		
//		Vector3 e_Pos = Vector3.vecSum(e_Dis, last_e_Pos);
//		
//		Rel.transpose();
//		
//		Vector3 GPS_e_Pos = WGS2ECEF(GPS_Pos, altitude);
//
//		k_Pos2 = Matrix.VecMult(Rel,GPS_e_Pos);
//		
////		k_Pos1.vecSum(k_Shift);
////		k_Pos2.vecSum(k_Shift); 
//		
//		last_Time = curr_Time;
//		last_Acc = b_Acc;
//		last_Vel = b_Vel;
//		last_Dis = b_Dis;
//		last_Pos = GPS_Pos;
	}
	
	public static Vector3 Integrate3Axis(Vector3 currPData, Vector3 lastPData, Vector3 lastData, double t1, double t2) 
	{
		
		/* A basic Trapezoidal rule code that numerically
		 *  integrates prime (P) data into data assuming 
		 * that the sampling frequency is 1. 
		 * integrate assumes an array with x, y, and z
		 * values
		 * Multiply result by the actual sampling frequency.
		 * Also, Add any known initial values to result
		 */
		
		Vector3 currData = new Vector3(0,0,0);
				
		// main update
		
		currData = Vector3.vecSum(Vector3.vecSum(currPData, lastPData).dotMult((t1-t2)/2.0),lastData);
		
		currData.rFix();
		
		return currData;
	}
	
	public static Matrix Attitude2RotMat(Vector3 PRY) 
	{
		/* 
		 * This method converts the attitude of a body
		 * into the local-body rotation matrix. Pitch, 
		 * Yaw (Azimuth), and Roll should all be in 
		 * radians.
		 */
		
		double[][] R_ = {{0,0,0},{0,0,0},{0,0,0}};
		
		Matrix R = new Matrix(R_);
		
		double sinP = Math.sin(PRY.x);
		double cosP = Math.cos(PRY.x);
		double sinR = Math.sin(PRY.y);
		double cosR = Math.cos(PRY.y);
		double sinY = Math.sin(PRY.z);
		double cosY = Math.cos(PRY.z);
		
		R.r1[0] = cosY*cosR - sinY*sinP*sinR;
		R.r1[1] = -cosP*sinY;
		R.r1[2] = cosY*sinR - cosY*sinP*cosR;
		R.r2[0] = sinY*cosR - cosY*sinP*sinR;
		R.r2[1] = cosP*cosY;
		R.r2[2] = sinY*sinR - cosY*sinP*cosR;
	    R.r3[0] = -cosP*sinR;
		R.r3[1] = sinP;
		R.r3[2] = cosP*cosR;
		
		R.fixCol();
		
		return R;
	}
	
	public static Vector3 RotMat2Attitude(Matrix R) 
	{
		/* 
		 * This method converts the local-body 
		 * rotation matrix of a body into the
		 * attitude of the body in radians.
		 */
		
		Vector3 PRY = new Vector3(0,0,0);
		
		PRY.x = Math.atan2(R.r2[0],R.r1[0]);
		PRY.y = Math.atan2(-R.r3[0],Math.sqrt(Math.pow(R.r3[1], 2) + Math.pow(R.r3[2], 2)));
		PRY.z = Math.atan2(R.r3[1],R.r3[2]);
		
		PRY.rFix();
		
		return PRY;
	}
	
	public static Matrix ECEFRotMatrix(GpsCoordinate GPS_Pos) 
	{
		
		/* Converts latitude and longitude into the
		 * Earth Centered, Earth Fixed (ECEF) 
		 * rotation matrix.
		 */
		
		double[][] Rel_ = {{0,0,0},{0,0,0},{0,0,0}};
		
		Matrix Rel = new Matrix(Rel_);
		
		Rel.r1[0] = -Math.sin(GPS_Pos.lon);
		Rel.r2[0] = Math.cos(GPS_Pos.lon);
		Rel.r1[1] = -Math.sin(GPS_Pos.lat)*Math.cos(GPS_Pos.lon);
		Rel.r2[1] = -Math.sin(GPS_Pos.lat)*Math.sin(GPS_Pos.lon);
		Rel.r3[1] = Math.cos(GPS_Pos.lat);
		Rel.r1[2] = Math.cos(GPS_Pos.lat)*Math.cos(GPS_Pos.lon);
		Rel.r2[2] = Math.cos(GPS_Pos.lat)*Math.sin(GPS_Pos.lon);
		Rel.r3[2] = Math.sin(GPS_Pos.lat);
		
		Rel.fixCol();
		
		return Rel;
	}
	
	
	public static GpsCoordinate ECEF2WGS(Vector3 e_Pos) 
	{
		
		/* Converts ECEF frame coordinates into 
		 * longitude[0] and latitude[1].
		 */
		
		GpsCoordinate WGS_Pos = new GpsCoordinate(0,0);
		
		
		// Setting up some common calculations
		// from the WGS-84 constants.
		double pp = Math.sqrt(Math.pow(e_Pos.x,2)+Math.pow(e_Pos.y,2));
		double theta = Math.atan(e_Pos.z*aa/(pp*bb));
		double ep = Math.sqrt((Math.pow(aa, 2) - Math.pow(bb, 2))/Math.pow(bb, 2));
		
		// Latitude
		WGS_Pos.lat = Math.atan((e_Pos.z + Math.pow(ep, 2)*bb*Math.pow(Math.sin(theta),3))/(pp-Math.pow(ee, 2)*aa*Math.pow(Math.cos(theta),3)));
		// Longitude
		WGS_Pos.lon = 2*Math.atan(e_Pos.y/(e_Pos.x+pp));
		/*
		* Currently, altitude does not back convert properly
		* it doesn't need to though so i'm leaving it commented 
		* out for now.
		*/
		
//		// Doing a sizable calculation in advance
//		double NN = Math.pow(aa,2)/Math.sqrt(Math.pow(aa,2)*Math.pow(Math.cos(WGS_Pos[1]),2) + Math.pow(bb,2)*Math.pow(Math.sin(WGS_Pos[1]),2));
//		
//		// Altitude
//		WGS_Pos[2] = pp/Math.cos(WGS_Pos[1]) - NN;
		
		return WGS_Pos;
	}
	
	public static Vector3 WGS2ECEF(GpsCoordinate WGS_Pos, double alti) 
	{
		
		/* Converts longitude, latitude, and altitude 
		 * into ECEF coordinates.
		 */
		
		Vector3 e_Pos = new Vector3(0,0,0);
		
		// Doing a sizable calculation in advance
		double RN = aa/(1-Math.pow(ee,2)*Math.pow(Math.sin(WGS_Pos.lat), 2));
		
		// x
		e_Pos.x = (RN + alti)*Math.cos(WGS_Pos.lat)*Math.cos(WGS_Pos.lon);
		// y
		e_Pos.y = (RN + alti)*Math.cos(WGS_Pos.lat)*Math.sin(WGS_Pos.lon);
		// z
		e_Pos.z = (RN*(1 - Math.pow(ee, 2)) + alti)*Math.sin(WGS_Pos.lat);
		
		e_Pos.rFix();
		
		return e_Pos;
	}

}
