package mappingbotapp.model;

/*
* Interface for a Data Store listener. The listener receives updates when the
* Data Store changes.
*/
public interface IDataStoreListener
{
    public void onDataChange();
}
