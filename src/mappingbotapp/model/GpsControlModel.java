package mappingbotapp.model;

import mappingbotapp.model.MotorCommand;
import mappingbotapp.generic.Vector2;
import mappingbotapp.generic.CoordinateList;
import mappingbotapp.generic.Vector3;
import java.util.concurrent.Callable;
import javafx.application.Platform;
import javafx.util.Pair;
import mappingbotapp.generic.AbstractCallback;

/*
* This class implements the Control model's public methods to provide mock
* data for testing and development purposes.
*/
public class GpsControlModel implements IControlModel, Runnable
{
    private IDataStoreModel dataStore;
    private ICommunicationModel communicationModel;
    
    private EstimatedPosition estimatedPos = new EstimatedPosition();
    
    public CoordinateList Path = new CoordinateList();
//    public BotState state = new BotState();
    
    public Vector2 currPos = new Vector2(0,0);
    
    public Vector2 prevPos = new Vector2(0,0);
    
    public Vector2 currSpeed = new Vector2(0,0);
    
    public Vector2 nextPoint = new Vector2(0,0);
    
    public Vector2 displacementFromPoint = new Vector2(0,0);
    
    public int pathLength;
    
    public int currIndex = 0;
    
    private double initHeadingAdjust; // Initial angle required to turn
    
    private double headingAdjustOld;
    
    private double distToAccel = 0; // Distance to accelerate to max speed
    
    private static double headingTolerance = 2.0*Math.PI/180.0; // 10 degree tolerance for heading difference
    private static float stopTolerance = 1.0f; // Tolerance to begin stopping when moving toward point
    private static float stopFastTolerance = 0.5f; // m/s
    private static float fullyStoppedTolerance = 0.25f; // Tolerance to determine when boat is fully stopped
    private static float MaxSpeed = 0.5f; // Hard set max speed of the bot. Semi-arbitrary.
    private static float targetTolerance = 0.5f; // Tolerance for being close enough to a point
    
    private Vector2 coordShift = new Vector2(0,0);
    
    private boolean isFirstDataSet = true;
    
    private boolean isMapping = false;
    
    private boolean isTurning = false;
    
    private boolean isMoving = false;
    
    private boolean isAccelerating = false;
    
    private MoveCommandType currentMoveCommandType = MoveCommandType.STOP;
    private long moveCommandLastUpdate = 0;
    private BotLocationData lastDataReceived = null;
    private long lastDataReceivedTime = 0;
    
    // Pointer to method to call when done mapping
    private AbstractCallback finishedCallback = null;
    
    @Override
    public boolean isMapping()
    {
        return isMapping;
    }
    
    public GpsControlModel(IDataStoreModel dataStore)
    {
        this.dataStore = dataStore;
        //Thread th = new Thread(this);
        //th.start();
    }
    
    @Override
    public void stopMapping()
    {
        isMapping = false;
    }
    
    @Override
    public void handleDisconnect()
    {
        isMapping = false;
    }
    
    @Override
    public void setDataStore(IDataStoreModel dataStore_)
    {
        dataStore = dataStore_;
    }
    
    @Override
    public void setCommunicationModel(ICommunicationModel comms)
    {
        communicationModel = comms;
    }
    
    @Override
    public void handleStartMappingCommand(AbstractCallback successCallback, 
            AbstractCallback failureCallback, AbstractCallback finishedCallback)
    {
            estimatedPos.reset();
            isMapping = true;

            // Call the success callback only when mapping is complete
            successCallback.call(null);
            
            this.finishedCallback = finishedCallback;
    }
    
    @Override
    public void handleStopMappingCommand(AbstractCallback successCallback, 
            AbstractCallback failureCallback)
    {
        // Make sure when we stop, that the boat stops too!
        communicationModel.sendMotorCommand(new MotorCommand(MotorCommand.Speed.OFF, MotorCommand.Direction.FORWARD),
                new MotorCommand(MotorCommand.Speed.OFF, MotorCommand.Direction.FORWARD));
        isMapping = false;
        isFirstDataSet = true;
        isMoving = false;
        isAccelerating = false;
        isTurning = false;
        successCallback.call(null);
    }
    
    @Override
    public void handleNewCoordinate(Vector3 coord)
    {
        // Pass the coord onto the data store
        if (dataStore != null)
        {
            // Need to run later because this can be in a non-UI thread
            Platform.runLater(new Runnable()
            {
                @Override
                public void run()
                {
                    dataStore.addCoordinate(coord);
                }
            });
        }
    }
    
    @Override
    public void returnToOrigin()
    {
    	nextPoint.x = 0.0;
    	nextPoint.y = 0.0;
    	nextPoint.PolarFix();
    	currIndex = Path.list.size();
    }
    
    @Override
    public void handleCalibration(BotLocationData data)
    {
//    	if ((CalibrationPhase == 0) | (CalibrationPhase == 3))
//    	{
//    		
//    		if (isFirstDataSet)
//    		{
//    			state.InitialState(data);
//    			currPos = state.k_Pos1.toCoord();
//    			isFirstDataSet = false;
//    		}
//    		else
//    		{
//    			state.UpdateReadings(data);
//    			currPos = state.k_Pos1.toCoord();
//    		}
//    		
//    		while (state.l_Vel.xyMag() > stopTolerance)
//    		{
//    			AccelerateBackwardMax();
//    		}
//    		
//    		MotorsOff();
//			isMoving = false;
//    		
//    		if (CalibrationPhase == 0)
//    		{
//        		CalibrationPhase++;
//    		}
//    		else
//    		{
//    			AccelerateBackwardMax();
//    			CalibrationPhase++;
//    		}
//    	}
//    	else
//    	{
//    		state.UpdateReadings(data);
//			currPos = state.k_Pos1.toCoord();
//			
//    		if (CalibrationPhase == 1)
//    		{
//    			AccelerateForwardMax();
//    			CalibrationPhase++;
//    		}
//    		else if (CalibrationPhase == 2)
//    		{
//    			CalibrationData[0] = state.l_Acc.xyMag();
//    			
//    			if (state.l_Acc.xyMag() > stopTolerance)
//    			{
//    				AccelerateForwardMax();
//    			}
//    			else 
//    			{
//    				CalibrationData[1] = state.l_Vel.xyMag();
//    				CalibrationPhase++;
//    			}
//    		}
//    		else if (CalibrationPhase == 4)
//    		{
//    			CalibrationData[2] = -state.l_Acc.xyMag();
//    			AccelerateForwardMax();
//    			CalibrationPhase++;
//    		}
//    		else
//    		{
//    			if (state.l_Vel.xyMag() > stopTolerance)
//    			{
//    				AccelerateForwardMax();
//    			}
//    			else
//    			{
//    				System.out.println("Calibration complete");
//    			}
//    		}
//    	}
    }
    
    // Handle reception of new location data
    @Override
    public void handleBotLocationData(BotLocationData data)
    {
        Vector3 dataToStore = new Vector3(data.position.x, data.position.y, 
                            data.depth);
        dataStore.addCoordinate(dataToStore);
        
        //lastDataReceived = data;
        //lastDataReceivedTime = System.currentTimeMillis();
        
        // Update estimated heading
        // System.out.println("Heading=" + data.rotation.x);
        //estimatedPos.setHeading(data.rotation.x);
        
        // Update position based on GPS if error is too large
        /*if (false)
    	{
            if(isFirstDataSet)
            {
                coordShift = data.gpsCoordinate;
                isFirstDataSet = false;
                System.out.println("STARTING MAPPING");
                return;
            }
        
            currPos = Coordinate.displacement(data.gpsCoordinate,coordShift);
            System.out.println("Current position: " + currPos);
    	
            currSpeed = Coordinate.displacement(currPos, prevPos);
    	
            displacementFromPoint = Coordinate.displacement(currPos, nextPoint);
            double headingAdjust = CheckDesiredHeading();
            headingAdjust = headingAdjust - data.rotation.x;
            double disToTarg = displacementFromPoint.r;

            if (!isMoving)
            {
                    if (headingAdjust > headingTolerance)  
                    {
                            if (!isTurning)
                            {
                                System.out.println("Truning to next point " + headingAdjust);
                                    StartHeadingAllign(headingAdjust);
                                    isTurning = true;
                                    initHeadingAdjust = headingAdjust;
                                    headingAdjustOld = headingAdjust;
                                    prevPos = currPos;
                                    return;
                            }
                            else
                            {
                                System.out.println("Still turning... "  + headingAdjust);
                                ContinueHeadingAllign(headingAdjust);
                                headingAdjustOld = headingAdjust;
                                return;
                            }
                    }
                    else if (Math.abs(headingAdjust - headingAdjustOld) < headingTolerance/2.0)
                    {
                        MotorsOff();
                        isTurning = false;
                        System.out.println("Done turning");
                        return;
                    }

                    if (currSpeed.r < MaxSpeed)
                    {
                            if (distToAccel != 0) 
                            {
                                    distToAccel = disToTarg;
                            }
                            System.out.println("Accelerating to speed");
                            AccelerateForwardMax();
                            prevPos = currPos;
                            return;
                    }
                    else
                    {
                            distToAccel = distToAccel - disToTarg;
                            MaintainSpeed();
                            System.out.println("At speed");
                            isMoving = true;
                    }
            }
            else
            {
                    if (disToTarg <= distToAccel)
                    {
                            if (currSpeed.r < stopTolerance)
                            {
                                    if (disToTarg < targetTolerance) 
                                    {
                                            MotorsOff();

                                            if (dataStore != null)
                                    {
                                        System.out.println("At Point. Storing data");
                                                    dataStore.addCoordinate(currPos.depthPair(data.depth));
                                    }

                                            isMoving = false;
                                            currIndex++;
                                            if (currIndex < Path.list.size()) 
                                            {
                                                    nextPoint = Path.list.get(currIndex);
                                            }
                                            else
                                            {
                                                    // TODO Mapping complete here
                                                    isMapping = false;
                                                    prevPos = currPos;
                                                    return;
                                            }

                                            distToAccel = 0;
                                    }
                                    else if(!isAccelerating)
                                    {
                                            AccelerateForwardLow();
                                            isAccelerating = true;
                                            System.out.println("Stopped early, fixing.");
                                    }
                                    else
                                    {
                                            AccelerateBackwardLow();
                                            isAccelerating = false;
                                    }
                            }
                            else
                            {
                                    AccelerateBackwardMax();
                                    System.out.println("Stopping...");
                            }
                    }
                    else if (headingAdjust > headingTolerance)
                    {
                            MovingHeadingAllign(headingAdjust);
                            System.out.println("Correcting heading while moving");
                    }
                    else
                    {
                            MaintainSpeed();
                            System.out.println("Holding speed");
                    }
            }
    	}
    	prevPos = currPos;*/
    }
    
    @Override
    public int getIndex()
    {
    	return currIndex;
    }
    
    public double CheckDesiredHeading() 
    {
    	double desiredHeading = 0.0;
    	
    	if (displacementFromPoint.x <= 0)
    	{
            //System.out.println("positive displacement " + Math.atan(displacementFromPoint.y/displacementFromPoint.x));
            desiredHeading = Math.PI/2 - Math.atan2(displacementFromPoint.y,displacementFromPoint.x);
    	}
    	else
    	{
            //System.out.println("negative displacement " + Math.atan(displacementFromPoint.y/displacementFromPoint.x));
            desiredHeading = Math.PI/2 - Math.atan2(displacementFromPoint.y,displacementFromPoint.x);
    	}
    	
    	return desiredHeading;
    }
    
    public void StartHeadingAllign(double headingAdjust)
    {
    	if (Math.sin(headingAdjust) > 0)
    	{
            TurnRight();
    	}
    	else if (Math.sin(headingAdjust) < 0)
    	{
            TurnLeft();
    	}
    	else if (Math.cos(headingAdjust) < 0)
    	{
            TurnRight();
    	}
    }
    
    public void ContinueHeadingAllign(double headingAdjust)
    {
        if (headingAdjust <= initHeadingAdjust/2.0)
        {
                StartHeadingAllign(-headingAdjust);
        }
        else
        {
                StartHeadingAllign(headingAdjust);
        }
    }
    
    public void MovingHeadingAllign(double headingAdjust)
    {
    	if (Math.sin(headingAdjust) > 0)
    	{
            MovingRightTurn();
    	}
    	else
    	{
            MovingLeftTurn();
    	}
    }
    
    public boolean TooFast()
    {
    	return currSpeed.r > MaxSpeed;
    }
    
    public void MaintainSpeed()
    {
    	if (TooFast()) 
    	{
            MotorsOff();
    	}
    	else
    	{
            AccelerateForwardLow();
    	}
    }
    
    @Override
    public int handlePathGeneration(int N, int M, double dx, double dy, Vector2 startPos, double phi)
    {
    	/* 
    	 * Generates a Boustrophedon path for the bot
    	 * of N by M points and with dx and dy spacing.
    	 * The hole set can be rotated about (0, 0) by 
    	 * headingAdjust and then shifted to start at startPos.
    	 */
    	
    	for (int i = 0; i < N; i++)
    	{
    		for (int j = 0; j < M; j++)
    		{
    			if (i%2==0) {
    				Path.list.add(new Vector2(dx*i, dy*j));
    			}
    			else
    			{
    				Path.list.add(new Vector2(dx*i, dy*(M - 1 - j)));
    			}
    		}
    	}
    	Path.Rotate(phi);
    	Path.Shift(startPos);
    	nextPoint = Path.list.get(0);
    	return N*M;
    }
    
    /** Motion Commands **/
    
    public void TurnLeft()
    {
        processMoveCommand(MoveCommandType.LEFT);
    }
    
    public void MovingLeftTurn()
    {
        processMoveCommand(MoveCommandType.MOVING_LEFT);
    }
    
    public void TurnRight()
    {
        processMoveCommand(MoveCommandType.RIGHT);
    }
    
    public void MovingRightTurn()
    {
        processMoveCommand(MoveCommandType.MOVING_RIGHT);
    }
    
    public void AccelerateForwardMax()
    {
        // Currently no distinction between max & low
        processMoveCommand(MoveCommandType.FORWARD);
    }
    
    public void AccelerateForwardLow()
    {
        // Currently no distinction between max & low
        processMoveCommand(MoveCommandType.FORWARD);
    }
    
    public void AccelerateBackwardMax()
    {
        // Currently no distinction between max & low
        processMoveCommand(MoveCommandType.REVERSE);
    }
    
    public void AccelerateBackwardLow()
    {
        // Currently no distinction between max & low
        processMoveCommand(MoveCommandType.REVERSE);
    }
    
    public void MotorsOff()
    {
        processMoveCommand(MoveCommandType.STOP);
    }
    
    @Override
    public void processMoveCommand(MoveCommandType commandType)
    {
        // Make sure it's been at least 1s since last command of the same type,
        // so the bot doesn't get flooded with commands
        long time = System.currentTimeMillis();
        if (commandType == this.currentMoveCommandType && 
                ((time - moveCommandLastUpdate) < 1000))
        {
            return;
        }
        
        moveCommandLastUpdate = time;
        
        // Process a move command by constructing two motor commands and forwarding
        // them to be communicated
        MotorCommand leftMotorCmd = new MotorCommand(MotorCommand.Speed.OFF, MotorCommand.Direction.FORWARD);
        MotorCommand rightMotorCmd = new MotorCommand(MotorCommand.Speed.OFF, MotorCommand.Direction.FORWARD);
        
        MotorCommand.Speed defaultSpeed = MotorCommand.Speed.MED_SPEED;
        
        switch (commandType)
        {
            case STOP:
                break;
            case FORWARD:
                leftMotorCmd.speed = defaultSpeed;
                rightMotorCmd.speed = defaultSpeed;
                break;
            case REVERSE:
                leftMotorCmd.speed = defaultSpeed;
                rightMotorCmd.speed = defaultSpeed;
                leftMotorCmd.direction = MotorCommand.Direction.REVERSE;
                rightMotorCmd.direction = MotorCommand.Direction.REVERSE;
                break;
            case LEFT:
                rightMotorCmd.speed = defaultSpeed;
                leftMotorCmd.speed = defaultSpeed;
                leftMotorCmd.direction = MotorCommand.Direction.REVERSE;
                break;
            case RIGHT:
                leftMotorCmd.speed = defaultSpeed;
                rightMotorCmd.speed = defaultSpeed;
                rightMotorCmd.direction = MotorCommand.Direction.REVERSE;
                break;
            case MOVING_LEFT:
                // Not used
                break;
            case MOVING_RIGHT:
                // Not used
                break;
        }
        
        communicationModel.sendMotorCommand(leftMotorCmd, rightMotorCmd);
        currentMoveCommandType = commandType;
    }
    
     @Override
    public void run()
    {
        // Continually update position estimate based on currently active command
        while (true)
        {
            if (dataStore != null)
            {
                long startTime = 0;
                long endTime = 0;
                // Get estimated position
                try
                {
                    if (startTime == 0)
                        startTime = System.currentTimeMillis();
                    else
                        startTime = endTime;
                    // Add some wait time before calculating next increment
                    Thread.sleep(10);
                    endTime = System.currentTimeMillis();

                    // Update the estimaed position based on time and current command
                    estimatedPos.update(endTime - startTime, new MoveCommand(currentMoveCommandType));
                    dataStore.addEstimatedPosition(estimatedPos.getVector());
                    dataStore.setHeading(estimatedPos.getHeading());
                }
                catch (InterruptedException e)
                {
                    // Do nothing
                }

                // Process automated control based on the estimated pos
                if (isMapping && currIndex == 0)
                {
                    // If we are mapping and at the first point, can skip to next point right away
                    // since we assume the starting position is (0,0)
                    if (lastDataReceived == null)
                    {
                        // At first point & waiting for first reading;
                    }
                    else
                    {
                        // Got first reading; adding to data store
                        Vector3 dataToStore = new Vector3(estimatedPos.getCoord().x, estimatedPos.getCoord().y, 
                            lastDataReceived.depth);
                        dataStore.addCoordinate(dataToStore);
                        currIndex++;
                    }
                }
                else if (isMapping && currIndex == Path.list.size())
                {
                    // Done mapping (reached the last point)
                    isMapping = false;
                    finishedCallback.callLater(null);
                }
                else if (isMapping)
                {
                    // At some point between start and end
                    // So check if we are close enough yet
                    Vector2 targetCoord = Path.list.get(currIndex);

                    displacementFromPoint = Vector2.displacement(targetCoord, estimatedPos.getCoord());
                    double distToTarg = displacementFromPoint.r;
                    if (distToTarg < targetTolerance)
                    {
                        System.out.println("Close enough; moving to next point.");
                        Vector3 dataToStore = new Vector3(estimatedPos.getCoord().x, estimatedPos.getCoord().y, 
                            lastDataReceived.depth);
                        dataStore.addCoordinate(dataToStore);
                        currIndex++;
                    }
                    else
                    {
                        // Not close enough yet. Need to move toward the target.
                        // First, check if heading is within tolerance; if so, move forward
                        double desiredHeading = CheckDesiredHeading();
                        double headingDelta = (desiredHeading - estimatedPos.getHeading());
                        double headingToleranceAdj = headingTolerance * Math.atan(targetTolerance/distToTarg);
                        headingToleranceAdj = Math.max(headingToleranceAdj, headingTolerance);
                        headingToleranceAdj = Math.min(Math.PI/4, headingToleranceAdj);
                        if (headingDelta > Math.PI)
                        {
                            headingDelta = -2*Math.PI + headingDelta;
                        }
                        else if (headingDelta < -Math.PI)
                        {
                            headingDelta = 2*Math.PI + headingDelta;
                        }
                        //System.out.println("heading=" + (estimatedPos.getHeading()) + ", desiredHeading=" + desiredHeading + ", headingDelta=" + headingDelta);
                        if (Math.abs(headingDelta) < headingToleranceAdj)  
                        {
                            // Within tolerance => move forward
                            // Or coast if pretty close
                            if (displacementFromPoint.r < stopTolerance)
                            {
                                //System.out.println("Coasting toward target " + targetCoord);
                                processMoveCommand(MoveCommandType.STOP);
                            }
                            else
                            {
                                //System.out.println("Moving toward target " + targetCoord);
                                processMoveCommand(MoveCommandType.FORWARD);
                            }
                        }
                        else
                        {
                            // If not within tolerance and not close, stop.
                            // Once velocity is low enough, turn.
                            if (estimatedPos.getVelocity().r < fullyStoppedTolerance)
                            {
                                // Velocity is low enough; turn
                                if (Math.sin(headingDelta) < 0) // headingDelta > 0
                                {
                                    System.out.println("Turning left... toward target " + targetCoord);
                                    processMoveCommand(MoveCommandType.LEFT);
                                }
                                else
                                {
                                    System.out.println("Turning right... toward target " + targetCoord);
                                    processMoveCommand(MoveCommandType.RIGHT);
                                }
                            }
                            else
                            {
                                // Velocity not low enough yet; remain stopped
                                System.out.println("Stopping so we can turn.");
                                processMoveCommand(MoveCommandType.STOP);
                            }
                        }
                    }
                }
            }
        }
    }
    
    @Override
    public void reset()
    {
        isMapping = false;
        currIndex = 0;
    }
}