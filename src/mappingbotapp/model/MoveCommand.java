package mappingbotapp.model;

/*
* MoveCommand wraps MoveCommandType (a move order for the boat).
*/
public class MoveCommand
{
    private MoveCommandType type;
    
    public static final double MAX_SPEED = 1.05; // [m/s]
    public static final double ACCELERATION = 0.5; // [m/s^2]
    public static final double MAX_RADIAL_SPEED = 3.5; // [rad/s]
    public static final double RADIAL_ACCELERATION = 0.6; // [rad/s^2]
    
    public MoveCommand(MoveCommandType type)
    {
        this.type = type;
    }
    
    public MoveCommandType getType()
    {
        return type;
    }
}
