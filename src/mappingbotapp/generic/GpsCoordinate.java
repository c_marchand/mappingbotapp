package mappingbotapp.generic;

/*
* 2D Vector that represents 
* an ordered GPS coordinate 
*/
public class GpsCoordinate
{
    public double lon;
    public double lat;
    
    public GpsCoordinate(double lon_, double lat_)
    {
        lon = lon_;
        lat = lat_;
    }
    
    public void Deg2Rad()
    {
    	lon = lon*Math.PI/180;
    	lat = lat*Math.PI/180;
    }
    
    public void Rad2Deg()
    {
    	lon = lon*180/Math.PI;
    	lat = lat*180/Math.PI;
    }
    
    @Override
    public String toString()
    {
        return lon + "," + lat;
    }
}
