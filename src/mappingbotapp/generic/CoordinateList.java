package mappingbotapp.generic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CoordinateList implements Iterable<Vector2> 
{
    public List<Vector2> list = new ArrayList<Vector2>();

    public Iterator<Vector2> iterator() 
    {
        return list.iterator();
    }

    public void Shift(Vector2 c)
    {
        for (Vector2 p : list)
        {
            p.CoordShift(c);
        }
    }

    public void Rotate(double phi)
    {
        for (Vector2 p : list)
        {
            p.Rotate(phi);
        }
    }
}
