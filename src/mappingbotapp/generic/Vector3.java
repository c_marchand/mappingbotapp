package mappingbotapp.generic;

/*
* Generic 3D vector.
*/
public class Vector3
{
    public double x;
    public double y;
    public double z;
    public double[] r = new double[3];
    
    public Vector3(double x_, double y_, double z_)
    {
        x = x_;
        y = y_;
        z = z_;
        r[0] = x;
        r[1] = y;
        r[2] = z;
    }
    
    public void rFix()
    {
    	r[0] = x;
    	r[1] = y;
    	r[2] = z;
    }
    
    public void xyzFix()
    {
    	x = r[0];
    	y = r[1];
    	z = r[2];
    }
    
    public static Vector3 vecSum(Vector3 a, Vector3 b)
    {
    	Vector3 c = new Vector3(0,0,0);
    	
    	c.x = a.x + b.x;
    	c.y = a.y + b.y;
    	c.z = a.z + b.z;
    	c.rFix();
    	
    	return c;
    }
    
    public static Vector3 accelerationFix(Vector3 a) 
    {
    
    	Vector3 b = new Vector3(0,0,0);
    	
    	b.x = -a.x;
    	b.y = -a.y;
    	
    	b.rFix();
    	
    	return b;
    }
    
    public static Vector3 attitudeFix(Vector3 a)
    {
    	Vector3 b = new Vector3(0,0,0);
    	b.z = -a.z;
    	b.y = -a.y;
    	// if the heading was positive then subtract 180 degrees
    	if (a.x > 0)
    	{
    		b.x = a.x - Math.PI; 
    	}
    	// if negative add 180 degrees
    	else
    	{
    		b.x = a.x + Math.PI;
    	}
    	
    	b.rFix();
    	
    	return b;
    }
    
    public Vector3 dotMult(double n)
    {
    	Vector3 c = new Vector3(0,0,0);
    	c.x = x*n;
    	c.y = y*n;
    	c.z = z*n;
    	c.rFix();
    	
    	return c;
    }
    
    public double xyMag()
    {
    	return Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
    }
    
    public Vector2 toCoord()
    {
    	return new Vector2(x,y);
    }
    
    @Override
    public String toString()
    {
        return x + "," + y + "," + z;
    }
    
    @Override
    public boolean equals (Object obj)
    {
        if (obj instanceof Vector3)
        {
            Vector3 other = (Vector3) obj;
            // If the values are equal within 1e4, the vectors can be considered
            // equal
            if ((other.x - x) < 1e-4 &&
                (other.y - y) < 1e-4 &&
                (other.z - z) < 1e-4)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
