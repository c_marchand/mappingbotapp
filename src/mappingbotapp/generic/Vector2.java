package mappingbotapp.generic;

/*
* Represents a coordinate in 2D
* rectangular coordinates. Used
* specifically for points 
*/ 
public class Vector2
{
    public double x;
    public double y;
    public double r;
    public double theta;
    
    public Vector2(double x_, double y_)
    {
        x = x_;
        y = y_;
        
        //r = Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
        //theta = Math.atan2(y,x);
        PolarFix();
    }
    
    public void PolarFix()
    {
    	r = Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
        theta = (Math.PI/2) - Math.atan2(y,x);
        
        if (theta > Math.PI)
        {
            theta = theta - 2*Math.PI;
        }
        else if (theta < -Math.PI)
        {
            theta = theta + 2*Math.PI;
        }
    }
    
    public void CarteFix()
    {
    	y = r*Math.sin(Math.PI/2 - theta);
        x = r*Math.cos(Math.PI/2 - theta);
    }
    
    public void CoordShift(Vector2 coord) 
    {
    	/*
    	 * Shifts the coordinate with whatever
    	 * other coordinate is input.
    	 */
    	
    	x = x + coord.x;
    	y = y + coord.y;
    }
    
    public void Rotate(double phi)
    {
    	theta = theta + phi;
    	
    	CarteFix();
    }
    
    public Vector2 ScalerShift(double n) 
    {
    	/*
    	 * Shifts the coordinate by the
    	 * scalar value n in both the x 
    	 * and y directions.
    	 */
    	Vector2 newCoord = new Vector2(0,0);
    	
    	newCoord.x = x+n;
    	newCoord.y = y+n;
    	
    	newCoord.PolarFix();
    	
    	return newCoord;    			
    }
    
    public Vector2 Scale(double n)
    {
    	/*
    	 * Returns the coordinate with both
    	 * dimensions scaled by n.
    	 */
    	
    	Vector2 newCoord = new Vector2(0,0);
    	
    	newCoord.x = x*n;
    	newCoord.y = y*n;
    	
    	newCoord.PolarFix();
    	
    	return newCoord;
    }
    
    public static Vector2 displacement(Vector2 P,Vector2 Q)
    {
    	Vector2 r = new Vector2(0,0);
    	
    	r.x = P.x-Q.x;
    	r.y = P.y-Q.y;
    	
    	r.PolarFix();
    	
    	return r;
    }
    
    public Vector3 depthPair(double depth)
    {
    	return new Vector3(x,y,depth);
    }
    
    public String toString()
    {
        return "(" + x + ", " + y + ")";
    }
}
