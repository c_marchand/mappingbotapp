package mappingbotapp.generic;

import javafx.application.Platform;

/*
* Abstract class for callback method implementation. Also adds functionality
* to run on UI thread.
*/
public abstract class AbstractCallback<P>
{
    // Puts callback on queue to be handled later by UI thread
    public void callLater(P param)
    {
        Platform.runLater(new Runnable()
        {
            public void run()
            {
                call(param);
            }
        });
    }
    
    public abstract void call(P param);
}
