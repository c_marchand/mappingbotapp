package mappingbotapp.generic;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ByteUtil
{
    public static float bytesToFloat(byte[] bytes)
    {
        // 4 bytes to a float
        return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getFloat();
    }
    
    public static double bytesToDouble(byte[] bytes)
    {
        // 8 bytes to a double
        double converted = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getDouble();
        return converted;
    }
    
    public static long bytesToLong(byte[] bytes)
    {
        // 8 bytes to a long
        return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getLong();
    }
    
    // Check if the bit at position "pos" in byte "x" is set
    public static boolean isBitSet(byte x, int pos)
    {
        // AND with a 1 shifted to correct position to see if result is correct
        return (x & (1 << pos)) != 0;
    }
}
