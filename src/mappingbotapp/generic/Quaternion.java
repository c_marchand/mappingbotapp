package mappingbotapp.generic;

public class Quaternion 
{
    public double i;
    public double j;
    public double k;
    public double q0;

    public double[] q;

    public Quaternion(double x, double y, double z) 
    {
        i = x;
        j = y;
        k = z;

        q0 = Math.sqrt(Math.pow(i,2)+Math.pow(j,2)+Math.pow(k,2));

        q[0] = q0;
        q[1] = i;
        q[2] = j;
        q[3] = k;
    }

    public void qFix()
    {
        q0 = Math.sqrt(Math.pow(i,2)+Math.pow(j,2)+Math.pow(k,2));

        q[0] = q0;
        q[1] = i;
        q[2] = j;
        q[3] = k;
    }

    public void ijkFix()
    {
        i = q[1];
        j = q[2];
        k = q[3];

        q0 = Math.sqrt(Math.pow(i,2)+Math.pow(j,2)+Math.pow(k,2));
    }

    public static Matrix Quaternion2RotMat(Quaternion q)
    {
        /* 
         * Converts a unit quaternion into an
         * equivalent rotation matrix using the
         * '123' convention.
         */

        double[][] R_ = {{0,0,0},{0,0,0},{0,0,0}};

        Matrix R = new Matrix(R_);

        // Diagonal values
        R.r1[0] = Math.pow(q.i,2) - Math.pow(q.j,2) - Math.pow(q.k,2) + Math.pow(q.q0,2);
        R.r2[1] = Math.pow(q.j,2) - Math.pow(q.i,2) - Math.pow(q.k,2) + Math.pow(q.q0,2);
        R.r3[2] = Math.pow(q.k,2) - Math.pow(q.i,2) - Math.pow(q.j,2) + Math.pow(q.q0,2);

        // Positive 'triangle'
        R.r1[1] = 2.0*q.i*q.j + 2.0*q.k*q.q0;
        R.r2[2] = 2.0*q.j*q.k + 2.0*q.i*q.q0;
        R.r3[0] = 2.0*q.k*q.i + 2.0*q.j*q.q0;

        // Negative 'triangle'
        R.r2[0] = 2.0*q.i*q.j - 2.0*q.k*q.q0;
        R.r3[1] = 2.0*q.j*q.k - 2.0*q.i*q.q0;
        R.r1[2] = 2.0*q.k*q.i - 2.0*q.j*q.q0;

        R.fixCol();

        return R;
    }
}
