package mappingbotapp.generic;

public class Matrix {

    /*
     * A simple 3x3 matrix class that can have 
     * its values accessed by row or column.
     * Includes some basic methods.
     */

    public double[] r1;
    public double[] r2;
    public double[] r3;
    public double[] c1;
    public double[] c2;
    public double[] c3;
    
    public Matrix(double[][] R)
    {
        r1 = R[0];
        r2 = R[1];
        r3 = R[2];
        
        double[] c1_ = new double[3];
        double[] c2_ = new double[3];
        double[] c3_ = new double[3];
        
        for (int i = 0; i < 3; i++) 
        {
        	c1_[i] = R[i][0];
        	c2_[i] = R[i][1];
        	c3_[i] = R[i][2];
        }
        
        c1 = c1_;
        c2 = c2_;
        c3 = c3_;
    }
    
    public static Matrix MatMult(Matrix A,Matrix B) 
    {
    	double[][] C_ = {{0,0,0},{0,0,0},{0,0,0}};
    	Matrix C = new Matrix(C_);
    	for (int i = 0; i < 3; i++)
    	{
    		C.r1[i] = A.r1[i] * B.c1[i];
    		C.r2[i] = A.r2[i] * B.c2[i];
    		C.r3[i] = A.r3[i] * B.c3[i];
    		
    	}
    	return C;	
    }
    
    public static Vector3 VecMult(Matrix A,Vector3 b) 
    {
    	Vector3 c = new Vector3(0,0,0);

    	c.x = A.r1[0]*b.x + A.r1[1]*b.y + A.r1[2]*b.z;
    	c.y = A.r2[0]*b.x + A.r2[1]*b.y + A.r2[2]*b.z;
    	c.z = A.r3[0]*b.x + A.r3[1]*b.y + A.r3[2]*b.z;

    	c.rFix();
    	
    	return c;
    }
    
    public void transpose()
    {
    	double[] temp1 = r1;
    	double[] temp2 = r2;
    	double[] temp3 = r3;
    	
    	r1 = c1;
    	r2 = c2;
    	r3 = c3;
    	
    	c1 = temp1;
    	c2 = temp2;
    	c3 = temp3;
    }
    
    public void fixCol()
    {
       	c1[0] = r1[0];
       	c1[1] = r2[0];
       	c1[2] = r3[0];
       	c2[0] = r1[1];
       	c2[1] = r2[1];
       	c2[2] = r3[1];
       	c3[0] = r1[2];
       	c3[1] = r2[2];
       	c3[2] = r3[2];
    }
	
    public void fixRow()
    {
       	r1[0] = c1[0];
       	r1[1] = c2[0];
       	r1[2] = c3[0];
       	r2[0] = c1[1];
       	r2[1] = c2[1];
       	r2[2] = c3[1];
       	r3[0] = c1[2];
       	r3[1] = c2[2];
       	r3[2] = c3[2];
    }
    
    @Override
    public String toString()
    {
        return r1[0] + " " + r1[1] + " " + r1[2] + "\n" +
        	r2[0] + " " + r2[1] + " " + r2[2] + "\n" +
        	r3[0] + " " + r3[1] + " " + r3[2];
    } 
}
