package mappingbotapp.ui;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import mappingbotapp.model.IDataStoreListener;
import mappingbotapp.model.IDataStoreModel;
import mappingbotapp.generic.Vector3;

/*
* The data panel displays the bot's gathered data in various formats.
*/
public class DataPanelView implements IDataStoreListener
{
    private static final String DATA_PANEL_LABEL = "Data";
    // For some reason, setting the collapsible property for the panels to false
    // causes the panel labels to be incorrectly offset - this fixes that.
    private static final String PANEL_LABEL_COLLAPSIBLE_OFFSET = "    ";
    
    private TableView<Vector3> table = new TableView<Vector3>();
    private GraphView graph = null;
    
    public DataPanelView(Pane root, IDataStoreModel dataStore)
    {
        dataStore.setListener(this);
        
        TitledPane dataPane = new TitledPane();
        dataPane.setCollapsible(false);
        dataPane.setText(PANEL_LABEL_COLLAPSIBLE_OFFSET + DATA_PANEL_LABEL);
        root.getChildren().add(dataPane);
        
        // Add the grid for the data view section
        VBox dataGrid = new VBox(0);
        dataGrid.setPadding(new Insets(0, 0, 0, 0));
        dataPane.setContent(dataGrid);
        dataGrid.getStyleClass().add("data-panel");

        // Add the tabs to allow selection between viewing modes
        TabPane tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        Tab tableTab = new Tab();
        tableTab.setContent(table);
        tableTab.setText("Table");
        
        Tab graphTab = new Tab();
        VBox graphPane = new VBox(10);
        graphTab.setContent(graphPane);
        graphTab.setText("Graph");
        
        // Debug tab is commented out unless needed!
        Tab debugTab = new Tab();
        VBox debugPane = new VBox(10);
        debugTab.setContent(debugPane);
        debugTab.setText("Debug");
        
        tabPane.getTabs().add(graphTab);
        tabPane.getTabs().add(tableTab);
        tabPane.getTabs().add(debugTab); // debug
        
        dataGrid.getChildren().add(tabPane);
        
        setupGraph(graphPane, dataStore);
        setupTable(dataGrid, dataStore);
        setupDebug(debugPane, dataStore); // debug
    }
    
    // Set up the tabular display
    private void setupTable(Pane viewPane, IDataStoreModel dataStore)
    {
        table.setEditable(true);
 
        // Set up the X column
        TableColumn xCol = new TableColumn("X");
        xCol.setMinWidth(50);
        xCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Vector3, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Vector3, String> p)
            {
                return getStringPropertyFromValue(p.getValue().x);
            }
        });
 
        // Set up the Y column
        TableColumn yCol = new TableColumn("Y");
        yCol.setMinWidth(50);
        yCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Vector3, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Vector3, String> p) {
                return getStringPropertyFromValue(p.getValue().y);
            }
        });
 
        // Set up the Z column
        TableColumn zCol = new TableColumn("Depth");
        zCol.setMinWidth(50);
        zCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Vector3, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Vector3, String> p) {
                return getStringPropertyFromValue(p.getValue().z);
            }
        });
 
        // Set the table to use data from the data store
        table.setItems(dataStore);
        table.getColumns().addAll(xCol, yCol, zCol);
 
        viewPane.getChildren().add(table);
    }
    
    // Convert doubles to string for tabular display
    private static SimpleStringProperty getStringPropertyFromValue(double value)
    {
        // Format to two decimals
        NumberFormat formatter = new DecimalFormat("#0.00");     
        return new SimpleStringProperty(formatter.format(value));
    }
    
    private void setupGraph(Pane viewPane, IDataStoreModel dataStore)
    {
        graph = new GraphView(viewPane, dataStore);
    }
    
    private void setupDebug(Pane viewPane, IDataStoreModel dataStore)
    {
        final DebugView debugView = new DebugView(viewPane, dataStore);
    }
    
    @Override
    public void onDataChange()
    {
        graph.refresh();
    }
}
