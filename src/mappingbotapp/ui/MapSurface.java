package mappingbotapp.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import io.github.jdiemke.triangulation.DelaunayTriangulator;
import io.github.jdiemke.triangulation.NotEnoughPointsException;
import io.github.jdiemke.triangulation.Triangle3D;
import io.github.jdiemke.triangulation.Vector3D;

import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Point;
import org.jzy3d.plot3d.primitives.Polygon;

import mappingbotapp.model.IDataStoreModel;
import mappingbotapp.generic.Vector3;

/*
* The MapSurface class represents the 3d surface that the depth data forms
* when using the graphical display.
*/
public class MapSurface extends org.jzy3d.plot3d.primitives.Shape
{
    // This threshold is used to identify triangles that are co-linear by 
    // comparing it to the distance of each point from the mean
    private static final double COLINEAR_THRESHOLD = 0.1;
    
    // Ctors to forward to org.jzy3d.plot3d.primitives.Shape
    public MapSurface()
    {
        super();
    }
        
    // Ctors to forward to org.jzy3d.plot3d.primitives.Shape
    public MapSurface(List<Polygon> polygons)
    {
         super();
         add(polygons);
    }
    
    // Refresh the surface by rebuilding the polygon set
    public void refresh(IDataStoreModel dataStore)
    {
        // Create a polygon list that will be used to create the surface
        List<Polygon> polygons = new ArrayList<Polygon>();

        // Create list of points for triangulation
        Vector<Vector3D> pointSet = new Vector<Vector3D>();
        List<Vector3> coordList = dataStore;
        int i = 0;
        float zmax = 0;
        float zmin = 0;
        for (Vector3 coord : coordList)
        {
            if (coord.z < zmin)
                zmin = (float) coord.z;
            if (coord.z > zmax)
                zmax = (float) coord.z;
            pointSet.add(new Vector3D(coord.x, coord.y, coord.z, i));
            i++;
        }
        
        // Now create the triangles based on the points list using the (modified)
        // Delaunay Triangulation library
        try
        {
            DelaunayTriangulator delaunayTriangulator = new DelaunayTriangulator(pointSet);
            delaunayTriangulator.triangulate();

            // Get the resulting triangles
            List<Triangle3D> triangleSoup = delaunayTriangulator.getTriangles();
               
            for (Triangle3D tri : triangleSoup)
            {
                // We don't want to add sliver tris that sometimes appear at the
                // edges due to non-uniformly spaced points
                if (!isSliverTriangle(tri))
                {
                    // Create polygon for jzy3d surface if it's not a sliver
                    // triangle
                    Polygon polygon = new Polygon();
                    polygon.add(new Point( new Coord3d(tri.a.x, tri.a.y, tri.a.z) ));
                    polygon.add(new Point( new Coord3d(tri.b.x, tri.b.y, tri.b.z) ));
                    polygon.add(new Point( new Coord3d(tri.c.x, tri.c.y, tri.c.z) ));
                    polygons.add(polygon);
                }
            }
        }
        catch (NotEnoughPointsException e)
        {
            // Just wait until there are enough points collected
        }
        
        // See: https://github.com/freddy33/jzy3d/blob/master/src/demos/org/jzy3d/demos/surface/delaunay/IncrementalDelaunayDemo.java
        // for reason why this must be synchronized
        synchronized(this)
        {
            this.clear();
            this.add(polygons);
        }
        this.setFaceDisplayed(true);
        this.setWireframeDisplayed(true);
        this.setWireframeColor(org.jzy3d.colors.Color.BLACK);
        // Color map the graph
        // Optional:  surface.getBounds().getZmin(), surface.getBounds().getZmax()
        this.setColorMapper(new ColorMapper(new ColorMapRainbow(), 
                zmin-1, zmax, 
                new org.jzy3d.colors.Color(1, 1, 1, .99f)));
        
        this.getBounds().reset();
    }
    
    // Check if a triangle is a sliver triangle (all the points more or less
    // on the same X&Y line)
    private boolean isSliverTriangle(Triangle3D tri)
    {
        double avg_x = (tri.a.x + tri.b.x + tri.c.x) / 3;
        double avg_y = (tri.a.y + tri.b.y + tri.c.y) / 3;
                
        // We can find these by checking if all 3 points in the triangle
        // are along the same line, within a threshold
        // Could also double check the the tri is on one of the 4 edges of the graph
        if ((Math.abs(tri.a.x - avg_x) < COLINEAR_THRESHOLD) && (Math.abs(tri.b.x - avg_x) < COLINEAR_THRESHOLD) && (Math.abs(tri.c.x - avg_x) < COLINEAR_THRESHOLD) ||  
            (Math.abs(tri.a.y - avg_y) < COLINEAR_THRESHOLD) && (Math.abs(tri.b.y - COLINEAR_THRESHOLD) < 0.5) && (Math.abs(tri.c.y - COLINEAR_THRESHOLD) < 0.5))
        {
            return true;
        }
        return false;
    }
}
