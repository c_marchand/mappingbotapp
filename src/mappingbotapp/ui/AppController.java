package mappingbotapp.ui;

import java.io.File;

import javafx.scene.control.Alert;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import mappingbotapp.generic.AbstractCallback;
import mappingbotapp.generic.Vector2;
import mappingbotapp.model.ConnectionStatus;
import mappingbotapp.model.ICommunicationModel;
import mappingbotapp.model.IControlModel;
import mappingbotapp.model.ComPort;
import mappingbotapp.model.IDataStoreModel;
import mappingbotapp.model.MoveCommandType;

/*
* This class is in charge of handling user interaction with the program.
*/
public class AppController
{
    // Defines the available operation modes
    private enum OperationMode
    {
        AUTO,
        MANUAL
    }
    
    private final AppView view;
    
    private final IControlModel controlModel;
    private final ICommunicationModel commsModel;
    private final IDataStoreModel dataStore;
    
    private ConnectionStatus connectionStatus;
    private OperationMode operationMode;
    
    // Key released state for WASD control
    private boolean keyReleased = true;
    
    public AppController(IDataStoreModel dataStore_, AppView appView,
            IControlModel controlModel_, ICommunicationModel commsModel_)
    {
        view = appView;
        controlModel = controlModel_;
        commsModel = commsModel_;
        dataStore = dataStore_;
        
        operationMode = OperationMode.AUTO;
        
        // Add event handlers for button clicks
        ControlPanelView controlPanel = view.getControlPanel();
        controlPanel.setConnectBtnHandler(e -> handleConnectBtnClick());
        controlPanel.setDisconnectBtnHandler(e -> handleDisconnectBtnClick());
        controlPanel.setStartMappingBtnHandler(e -> handleStartMappingBtnClick());
        controlPanel.setStopMappingBtnHandler(e -> handleStopMappingBtnClick());
        controlPanel.setResetMappingBtnHandler(e -> handleResetMappingBtnClick());
        controlPanel.setAutomatedOperationBtnHandler(e -> handleAutoOperationClick());
        controlPanel.setManualOperationBtnHandler(e -> handleManualOperationClick());
        controlPanel.setMoveStopBtnHandler(e -> handleManualMoveClick(MoveCommandType.STOP));
        controlPanel.setMoveFwdBtnHandler(e -> handleManualMoveClick(MoveCommandType.FORWARD));
        controlPanel.setMoveRevBtnHandler(e -> handleManualMoveClick(MoveCommandType.REVERSE));
        controlPanel.setMoveLeftBtnHandler(e -> handleManualMoveClick(MoveCommandType.LEFT));
        controlPanel.setMoveRightBtnHandler(e -> handleManualMoveClick(MoveCommandType.RIGHT));
        
        // Link dropdown box to show available COM ports
        controlPanel.setAvailableComPortSource(commsModel_.getAvailableComPorts());
        
        // Add event handlers for menu items
        MenuBarView menuBarView = view.getMenuBarView();
        menuBarView.setMenuExportHandler(e -> handleExportClick());
        menuBarView.setMenuImportHandler(e -> handleImportClick());
        
        // Initialize buttons, labels
        toggleConnectedStatus(ConnectionStatus.NOT_CONNECTED);
        toggleMappingStatus(false);
        
        // Add handlers for WASD control
        view.getScene().addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
            if (keyReleased)
            {
                if (key.getCode()==KeyCode.W)
                {
                    keyReleased = false;
                    view.getControlPanel().clickMoveBtn(MoveCommandType.FORWARD, false);
                }
                else if (key.getCode()==KeyCode.A)
                {
                    keyReleased = false;
                    view.getControlPanel().clickMoveBtn(MoveCommandType.LEFT, false);
                }
                else if (key.getCode()==KeyCode.S)
                {
                    keyReleased = false;
                    view.getControlPanel().clickMoveBtn(MoveCommandType.REVERSE, false);
                }
                else if (key.getCode() == KeyCode.D)
                {
                    keyReleased = false;
                    view.getControlPanel().clickMoveBtn(MoveCommandType.RIGHT, false);
                }
                else if (key.getCode() == KeyCode.SHIFT)
                {
                    keyReleased = false;
                    view.getControlPanel().clickMoveBtn(MoveCommandType.STOP, false);
                }
            }
        });
        
        // Add key release handler for WASD control
        view.getScene().addEventHandler(KeyEvent.KEY_RELEASED, (key) -> {
            keyReleased = true;
        });
    }
    
    /*
    * This class is used when a start mapping command is successful.
    */
    class StartMappingSuccessHandler extends AbstractCallback<Void>
    {
        public void call(Void v)
        {
            toggleMappingStatus(true);
        }
    }
    
    /*
    * This class is used when mapping is complete.
    */
    class MappingCompleteHandler extends AbstractCallback<Void>
    {
        public void call(Void v)
        {
            view.showAlert(Alert.AlertType.INFORMATION, "Mapping session is complete!");
            toggleMappingStatus(false);
        }
    }
    
    /*
    * This class is used when a start mapping command is unsuccessful.
    */
    class StartMappingFailureHandler extends AbstractCallback<Void>
    {
        public void call(Void v)
        {
           view.showAlert(Alert.AlertType.ERROR, "Start Mapping command failed.");
        }
    }
    
    /*
    * This class is used when a stop mapping command is successful.
    */
    class StopMappingSuccessHandler extends AbstractCallback<Void>
    {
        public void call(Void v)
        {
            toggleMappingStatus(false);
            
            System.out.println("Stop Mapping command succeeded.");
        }
    }
    
    /*
    * This class is used when a stop mapping command is unsuccessful.
    */
    class StopMappingFailureHandler extends AbstractCallback<Void>
    {
        public void call(Void v)
        {
            view.showAlert(Alert.AlertType.ERROR, "Stop Mapping command failed.");
        }
    }
    
    /*
    * This class is used when a connect command is successful.
    */
    class ConnectSuccessHandler extends AbstractCallback<Void>
    {
        public void call(Void v)
        {
            toggleConnectedStatus(ConnectionStatus.CONNECTED);
        }
    }
    
    /*
    * This class is used when a connect command is unsuccessful.
    * the
    */
    class ConnectFailureHandler extends AbstractCallback<String>
    {
        public void call(String descr)
        {
            toggleConnectedStatus(ConnectionStatus.CONNECTION_FAILED);
            view.showAlert(Alert.AlertType.ERROR, "Connection was unsuccessful. The following error occured: \n\n" + descr);
        }
    }
    
    /*
    * This class is used when a disconnect command is successful.
    */
    class DisconnectSuccessHandler extends AbstractCallback<Void>
    {
        public void call(Void v)
        {
            toggleConnectedStatus(ConnectionStatus.NOT_CONNECTED);
        }
    }
    
    /*
    * This class is used when a disconnect command is unsuccessful.
    */
    class DisconnectFailureHandler extends AbstractCallback<String>
    {
        public void call(String descr)
        {
            view.showAlert(Alert.AlertType.ERROR, "Disconnecting was unsuccessful. An error occured: \n" + descr);
        }
    }
    
    /*
    * This class is used when an export command is successful.
    */
    class ExportSuccessHandler extends AbstractCallback<Void>
    {
        public void call(Void v)
        {
            view.showAlert(Alert.AlertType.INFORMATION, "Export was successful.");
        }
    }
    
    /*
    * This class is used when an export command is unsuccessful.
    */
    class ExportFailureHandler extends AbstractCallback<Void>
    {
        public void call(Void v)
        {
            view.showAlert(Alert.AlertType.ERROR, "Export failed.");
        }
    }
    
    /*
    * This class is used when an import command is successful.
    */
    class ImportSuccessHandler extends AbstractCallback<Void>
    {
        public void call(Void v)
        {
            view.showAlert(Alert.AlertType.INFORMATION, "Import was successful.");
        }
    }
    
    /*
    * This class is used when an import command is unsuccessful.
    */
    class ImportFailureHandler extends AbstractCallback<Void>
    {
        public void call(Void v)
        {
            view.showAlert(Alert.AlertType.ERROR, "Import failed.");
        }
    }
    
    // Sends connect command to the communication model
    public void handleConnectBtnClick()
    {
        ComPort selectedPort = view.getControlPanel().getSelectedComPort();
        if (selectedPort != null)
        {
            toggleConnectedStatus(ConnectionStatus.ATTEMPTING_CONNECTION);
            commsModel.sendConnectCommand(selectedPort, new ConnectSuccessHandler(), new ConnectFailureHandler());
        }
        else
        {
            view.showAlert(Alert.AlertType.ERROR, "You must select a COM port before attempting to connect.");
        }
    }
    
    // Sends disconnect command to the communication model
    public void handleDisconnectBtnClick()
    {
        commsModel.sendDisconnectCommand(new DisconnectSuccessHandler(), new DisconnectFailureHandler());
    }
    
    // Sends "start mapping" command to the control model
    public void handleStartMappingBtnClick()
    {
        if (view.getControlPanel().getGridAreaX() >= 1 && 
            view.getControlPanel().getGridAreaX() >= 1)
        {
            controlModel.handlePathGeneration(view.getControlPanel().getGridAreaX()/2 + 1, view.getControlPanel().getGridAreaY()/2 + 1, 2, 2, new Vector2(0,0), 0);
            controlModel.handleStartMappingCommand(new StartMappingSuccessHandler(), new StartMappingFailureHandler(), new MappingCompleteHandler());
        }
        else
        {
            view.showAlert(Alert.AlertType.ERROR, "Grid must be at least 1x1");
        }
    }
    
    // Sends "stop mapping" command to the control model
    public void handleStopMappingBtnClick()
    {
        controlModel.handleStopMappingCommand(new StopMappingSuccessHandler(), new StopMappingFailureHandler());
    }
    
    // Reset mapping session (remove all data and reset
    public void handleResetMappingBtnClick()
    {
        dataStore.reset();
        controlModel.reset();
    }
    
    // Handle switch to auto operation mode
    public void handleAutoOperationClick()
    {
        operationMode = OperationMode.AUTO;
        
        if (isConnected())
        {
            // First, get the bot to stop moving
            view.getControlPanel().clickMoveBtn(MoveCommandType.STOP, true);

            // Then switch to automated mode
            view.getControlPanel().toggleManualControls(false);
            view.getControlPanel().toggleAutomatedControls(true);
        }
    }
    
    // Handle switch to manual operation mode
    public void handleManualOperationClick()
    {
        operationMode = OperationMode.MANUAL;
        
        if (isConnected())
        {
            // Stop the mapping session
            controlModel.stopMapping();
            view.getControlPanel().toggleAutomatedControls(false);
            view.getControlPanel().toggleManualControls(true);
        }
    }
    
    public void handleExportClick()
    {
        // Get the destination file
        File exportFile = view.getSelectedFile(true);
        
        // Do the export
        if (exportFile != null)
            dataStore.export(exportFile, new ExportSuccessHandler(), new ExportFailureHandler());
    }
    
    public void handleImportClick()
    {
        // Get the destination file
        File importFile = view.getSelectedFile(false);
        
        // Do the import
        if (importFile != null)
            dataStore.importFromFile(importFile, new ImportSuccessHandler(), new ImportFailureHandler());
    }
    
    public void handleManualMoveClick(MoveCommandType direction)
    {
        // Forward the move command to the control model
        controlModel.processMoveCommand(direction);
    }
    
    // Toggle the view to enable/disable buttons and labels depending on the current
    // connection status
    private void toggleConnectedStatus(ConnectionStatus status)
    {
        connectionStatus = status;
        boolean isConnected = isConnected();
        
        ControlPanelView panel = view.getControlPanel();
        panel.toggleConnectBtn(!isConnected);
        panel.toggleDisconnectBtn(isConnected);
        panel.toggleStartMappingBtn(isConnected);
        panel.toggleStopMappingBtn(false);
        panel.toggleConnectedStatus(status);
        panel.toggleAutomatedControls(isConnected && (operationMode == OperationMode.AUTO));
        panel.toggleManualControls(isConnected && (operationMode == OperationMode.MANUAL));
        
        if (!isConnected)
        {
            panel.toggleMappingStatus(isConnected);
        }
    }
    
    // Toggle view depending on whether or not device is currently mapping
    private void toggleMappingStatus(boolean isMapping)
    {
        ControlPanelView panel = view.getControlPanel();
        
        boolean startMappingEnabled = !isMapping && (connectionStatus == ConnectionStatus.CONNECTED);
        panel.toggleStartMappingBtn(startMappingEnabled);
        panel.toggleStopMappingBtn(isMapping);
        panel.toggleResetMappingBtn(!isMapping);
        panel.toggleMappingStatus(isMapping);
    }
    
    private boolean isConnected()
    {
        return (connectionStatus == ConnectionStatus.CONNECTED);
    }
}