package mappingbotapp.ui;

import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

import mappingbotapp.generic.GpsCoordinate;
import mappingbotapp.model.IDataStoreModel;

/*
* The DebugView displays the estimated path of the bot.
*/
public class DebugView implements ListChangeListener
{
    private final XYChart.Series chartPositions;
    private final ObservableList<GpsCoordinate> estimatedPositions;
    private final ScatterChart<Number,Number> sc;
            
    private final IDataStoreModel dataStore;
    
    private final Image headingImage;
    private final ImageView headingImageView;
    
    public DebugView(Pane viewPane, IDataStoreModel dataStore)
    {
        this.dataStore = dataStore;
        
        headingImage = new Image("/arrow.png");
        headingImageView = new ImageView();
        headingImageView.setImage(headingImage);
        headingImageView.setRotate(-90);
        viewPane.getChildren().add(headingImageView);
                
        estimatedPositions = dataStore.getEstimatedPositions();
        
        
        //this.dataStore = dataStore;
        final NumberAxis xAxis = new NumberAxis(-4, 15, 1);
        final NumberAxis yAxis = new NumberAxis(-4, 15, 1);      
        //new NumberAxis();
        sc = new
            ScatterChart<>(xAxis,yAxis);
        sc.autosize();
        xAxis.setLabel("X [m]");                
        yAxis.setLabel("Y [m]");
        sc.setTitle("Bot Estimated Position");
       
        chartPositions = new XYChart.Series();   
 
        sc.getData().addAll(chartPositions);
        
        viewPane.getChildren().add(sc);
        
        dataStore.getEstimatedPositions().addListener(this);
    }
    
    @Override
    public void onChanged(Change change)
    {
        boolean removed = false;
        while(change.next())
        {
            if (change.wasAdded())
            {
                //System.out.println("Changed");
                for (int i = change.getFrom(); i < change.getTo(); i++)
                {
                    GpsCoordinate newPos = estimatedPositions.get(i);
                    Platform.runLater(new Runnable()
                    {
                        public void run()
                        {
                            //System.out.println(newPos);
                            chartPositions.getData().add(new XYChart.Data(newPos.lon, newPos.lat));
                        }
                    });
                }
            }
           
            if (change.wasRemoved() || 
                (chartPositions.getData() != null && chartPositions.getData().size() > 10000))
            {
                // Assume that if any item was removed, this means the data 
                // store has been reset
                // chartPositions.getData().clear();
                Platform.runLater(new Runnable()
                {
                    public void run()
                    {
                        chartPositions.getData().clear();
                    }
                });
            }
        }
        
        if (sc != null)
            sc.autosize();
        
        if (dataStore != null)
        {
            double headingRad = -dataStore.getHeading() + Math.PI/2;
            double rotateDeg = (headingRad)*180/Math.PI;

            headingImageView.setRotate(-rotateDeg);
        }
    }
}
