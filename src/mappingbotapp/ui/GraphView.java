package mappingbotapp.ui;

import io.github.jdiemke.triangulation.Vector3D;
import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Insets;
import javafx.scene.layout.Pane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import mappingbotapp.generic.Vector3;

import org.jzy3d.chart.AWTChart;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.javafx.JavaFXChartFactory;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Polygon;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.jzy3d.plot3d.primitives.Point;

import mappingbotapp.model.IDataStoreModel;
import org.jzy3d.maths.BoundingBox3d;
import org.jzy3d.plot3d.rendering.view.modes.ViewBoundMode;

/*
* The graph view shows the gathered depth data in 3d format.
*/
public class GraphView
{
    private IDataStoreModel dataStore;
    
    protected AWTChart chart;
    
    protected ImageView imageView;
    private JavaFXChartFactory factory;
    private MapSurface surface;
    
    public GraphView(Pane viewPane, IDataStoreModel dataStore)
    { 
        this.dataStore = dataStore;

        // Initialize polygon list for the 3d surface
        List<Polygon> polygons = new ArrayList<Polygon>();
        // Jzy3d seems to require at least one polygon to start, so a polygon
        // that will be erased is created here
        Polygon polygon = new Polygon();
        polygon.add(new Point( new Coord3d(1, 1+1, 0.25) ));
        polygon.add(new Point( new Coord3d(1+1, 1+1, 0.25) ));
        polygon.add(new Point( new Coord3d(1+1, 1, 0.25) ));
        polygons.add(polygon);
        
        // Create the 3d surface
        surface = new MapSurface(polygons);
        surface.setColorMapper(new ColorMapper(new ColorMapRainbow(), surface.getBounds().getZmin(), surface.getBounds().getZmax(), new org.jzy3d.colors.Color(1, 1, 1, 0.99f)));
        surface.setFaceDisplayed(false);
        surface.setWireframeDisplayed(false);
        surface.setWireframeColor(org.jzy3d.colors.Color.BLACK);
        surface.getBounds().setZmax(0);

        // Create a chart
        factory = new JavaFXChartFactory();
        chart = (AWTChart) factory.chart(Quality.Advanced, "offscreen");
        chart.getScene().getGraph().add(surface);
        
        // Create the image view that holds the chart
        ImageView imageView_ = factory.bindImageView(chart);
        imageView_.setFitHeight(350);
        imageView_.setFitWidth(375);
        BorderPane imageViewWrapper = new BorderPane(imageView_);
        String css = "-fx-border-color: black;\n" +
                   "-fx-border-insets: 0;\n" +
                   "-fx-border-width: 1;\n";
                   //"-fx-border-style: dashed;\n";
        imageViewWrapper.setStyle(css);
        imageViewWrapper.prefHeightProperty().bind(imageView_.fitHeightProperty());
        imageViewWrapper.prefWidthProperty().bind(imageView_.fitWidthProperty());
        
        viewPane.getChildren().add(imageViewWrapper);
        viewPane.setPadding(new Insets(15));
    }
    
    // Refreshes the map display
    public void refresh()
    {
       
        
        // Update the bounds of the graph
        chart.getView().updateBounds();
        chart.getView().getScene().getGraph().getBounds().setXmax(20);
        chart.getView().setBoundMode(ViewBoundMode.MANUAL);
       
        List<Vector3> coordList = dataStore;
        int i = 0;
        float xmin = 0;
        float xmax = 1;
        float ymin = 0;
        float ymax = 1;
        float zmax = 1;
        float zmin = 0;
        for (Vector3 coord : coordList)
        {
            if (coord.x < xmin)
                xmin = (float) coord.x;
            if (coord.x > xmax)
                xmax = (float) coord.x;
            if (coord.y < ymin)
                ymin = (float) coord.y;
            if (coord.y > ymax)
                ymax = (float) coord.y;
            if (coord.z < zmin)
                zmin = (float) coord.z;
            if (coord.z > zmax)
                zmax = (float) coord.z;
            i++;
        }
        
         chart.getView().setBoundManual(new BoundingBox3d(xmin, xmax, ymin, ymax, zmin, zmax));
          // Refresh the 3d surface
        surface.refresh(dataStore);
    }
}
