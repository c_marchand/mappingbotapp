package mappingbotapp.ui;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Separator;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import mappingbotapp.model.ComPort;
import mappingbotapp.model.ConnectionStatus;
import mappingbotapp.model.MoveCommandType;

/*
* The control panel view shows the controls that allow the user to control the 
* boat.
*/
public class ControlPanelView
{
    private static final String CONTROL_PANEL_LABEL = "Control Panel";

    // For some reason, setting the collapsible property for the panels to false
    // causes the panel labels to be incorrectly offset - this fixes that.
    private static final String PANEL_LABEL_COLLAPSIBLE_OFFSET = "    ";
    
    private static final String CONNECT_BTN_LABEL = "Connect";
    private static final String DISCONNECT_BTN_LABEL = "Disconnect";
    private static final String START_MAPPING_BTN_LABEL = "Start Mapping";
    private static final String STOP_MAPPING_BTN_LABEL = "Stop Mapping";
    private static final String RESET_MAPPING_BTN_LABEL = "Reset";
    
    private static final String CONNECTION_STATUS_LABEL = "Connection Status :";
    private static final String COM_PORT_LABEL = "COM Port :";
    private static final String MAPPING_STATUS_LABEL = "Mapping Status :";
    
    private static final String CONNECTION_STATUS_TRUE = "Connected";
    private static final String CONNECTION_STATUS_FALSE = "Not Connected";
    private static final String CONNECTION_STATUS_ATTEMPTING = "Attempting Connection";
    private static final String CONNECTION_STATUS_FAILED = "Connection Failed";
    private static final String MAPPING_STATUS_TRUE = "Mapping";
    private static final String MAPPING_STATUS_FALSE = "Not Mapping";
    private static final Color  CONNECTION_STATUS_TRUE_COLOR = Color.GREEN;
    private static final Color  CONNECTION_STATUS_ATTEMPTING_COLOR = Color.ORANGE;
    private static final Color  CONNECTION_STATUS_FALSE_COLOR = Color.GREY;
    private static final Color  CONNECTION_STATUS_FAILED_COLOR = Color.RED;
    private static final Color  MAPPING_STATUS_TRUE_COLOR = Color.GREEN;
    private static final Color  MAPPING_STATUS_FALSE_COLOR = Color.GREY; 
    
    private static final int GRID_AREA_DEFAULT_SIZE = 2; // meters
    
    // Comms controls
    private final Button connectBtn;
    private final Button disconnectBtn;
    private final ComboBox comPortDropdown;
    private final Text connectionStatusValue;
    
    // Operating mode controls
    private final Text operatingModeLabel;
    private final RadioButton operatingModeAutoBtn;
    private final RadioButton operatingModeManualBtn;
    
    // Auto controls
    private final Button startMappingBtn;
    private final Button stopMappingBtn;
    private final Button resetMappingBtn;
    private final Text mappingStatusValue;
    private final NumericTextField gridSizeX;
    private final NumericTextField gridSizeY;
    
    // Manual controls
    private final ToggleButton moveLeftBtn;
    private final ToggleButton moveFwdBtn;
    private final ToggleButton moveRevBtn;
    private final ToggleButton moveRightBtn;
    private final ToggleButton moveStopBtn;
    
    public ControlPanelView(Pane root)
    {
        // Add the control panel section
        TitledPane controlPanel = new TitledPane();
        controlPanel.setCollapsible(false);
        controlPanel.setText(PANEL_LABEL_COLLAPSIBLE_OFFSET + CONTROL_PANEL_LABEL);
        root.getChildren().add(controlPanel);
        
        // And a grid for the contents of the control panel
        // Left side contains comms and status, right side contains controls
        HBox mainGrid = new HBox(10);
        
        VBox controlPanelGrid = new VBox(15);
        mainGrid.setPadding(new Insets(10, 10, 10, 10));
        controlPanel.setContent(mainGrid);
        mainGrid.getChildren().add(controlPanelGrid);
        
        // Add the connection status
        HBox connectionStatusBox = new HBox(10);
        controlPanelGrid.getChildren().add(connectionStatusBox);
        // Add the connection status label
        Text connectionStatusLabel = new Text();
        connectionStatusLabel.setText(CONNECTION_STATUS_LABEL);
        connectionStatusBox.getChildren().add(connectionStatusLabel);
        // Add the actual value
        connectionStatusValue = new Text();
        connectionStatusValue.setText(CONNECTION_STATUS_FALSE);
        connectionStatusValue.setFill(CONNECTION_STATUS_FALSE_COLOR);
        connectionStatusBox.getChildren().add(connectionStatusValue);
        
        // Add the COM port selection area
        HBox comPortBox = new HBox(10);
        controlPanelGrid.getChildren().add(comPortBox);
        // Add the label
        Text comPortLabel = new Text();
        comPortLabel.setText(COM_PORT_LABEL);
        comPortBox.getChildren().add(comPortLabel);
        // Add the dropdown
        comPortDropdown = new ComboBox();
        comPortBox.getChildren().add(comPortDropdown);
        
        // Add the buttons to the control panel
        HBox connectionBtnsBox = new HBox(10);
        connectBtn = new Button();
        connectBtn.setText(CONNECT_BTN_LABEL);
        connectionBtnsBox.getChildren().add(connectBtn);
        
        disconnectBtn = new Button();
        disconnectBtn.setText(DISCONNECT_BTN_LABEL);
        connectionBtnsBox.getChildren().add(disconnectBtn);
        controlPanelGrid.getChildren().add(connectionBtnsBox);
        
        // The disconnect button is disabled until the device is connected
        disconnectBtn.setDisable(true);
        
        // Add the right portion of the grid for controls
        VBox rightSideGrid = new VBox(15);
        rightSideGrid.getStyleClass().add("control-panel-right");
        controlPanelGrid.getStyleClass().add("control-panel-left");
        
        operatingModeLabel = new Text();
        operatingModeLabel.setText("Operating Mode: ");
        operatingModeAutoBtn = new RadioButton("Automated");
        operatingModeManualBtn = new RadioButton("Manual");
        ToggleGroup operatingModeGroup = new ToggleGroup();
        operatingModeAutoBtn.setToggleGroup(operatingModeGroup);
        operatingModeManualBtn.setToggleGroup(operatingModeGroup);
        operatingModeAutoBtn.setSelected(true);
        
        VBox operatingModeControls = new VBox(10);
        HBox operatingModeBox = new HBox(10);
        operatingModeControls.getChildren().add(operatingModeLabel);
        operatingModeBox.getChildren().add(operatingModeAutoBtn);
        operatingModeBox.getChildren().add(operatingModeManualBtn);
        operatingModeControls.getChildren().add(operatingModeBox);
        rightSideGrid.getChildren().add(operatingModeControls);
                
        HBox mappingBox = new HBox(10);
       
        // Add the automated controls label
        Text automatedControlsLabel = new Text("Automated Controls");
        automatedControlsLabel.setStyle("-fx-font-weight: bold;");
        rightSideGrid.getChildren().add(automatedControlsLabel);
        
        // Add the grid size input
        HBox gridSizeBox = new HBox(10);
        Text gridSizeLabel = new Text("Grid Area (meters):");
        gridSizeX = new NumericTextField();
        gridSizeX.setText(String.valueOf(GRID_AREA_DEFAULT_SIZE));
        gridSizeX.setPrefWidth(50);
        Text gridSizeByLabel = new Text("by");
        gridSizeY = new NumericTextField();
        gridSizeY.setPrefWidth(50);
        gridSizeY.setText(String.valueOf(GRID_AREA_DEFAULT_SIZE));
        gridSizeBox.getChildren().addAll(gridSizeLabel, gridSizeX, gridSizeByLabel, gridSizeY);
        rightSideGrid.getChildren().addAll(gridSizeBox);
        
        // Add the mapping status
        HBox mappingStatusBox = new HBox(10);
        rightSideGrid.getChildren().add(mappingStatusBox);
        Text mappingStatusLabel = new Text();
        mappingStatusLabel.setText(MAPPING_STATUS_LABEL);
        mappingStatusBox.getChildren().add(mappingStatusLabel);
        // Add the actual value
        mappingStatusValue = new Text();
        mappingStatusValue.setText(MAPPING_STATUS_FALSE);
        mappingStatusValue.setFill(MAPPING_STATUS_FALSE_COLOR);
        mappingStatusBox.getChildren().add(mappingStatusValue);
        
        startMappingBtn = new Button();
        startMappingBtn.setText(START_MAPPING_BTN_LABEL);
        mappingBox.getChildren().add(startMappingBtn);
        
        stopMappingBtn = new Button();
        stopMappingBtn.setText(STOP_MAPPING_BTN_LABEL);
        mappingBox.getChildren().add(stopMappingBtn);
        rightSideGrid.getChildren().add(mappingBox);
        
        resetMappingBtn = new Button();
        resetMappingBtn.setText(RESET_MAPPING_BTN_LABEL);
        mappingBox.getChildren().add(resetMappingBtn);
        
        // The mapping buttons are disabled until the device is connected
        stopMappingBtn.setDisable(true);
        startMappingBtn.setDisable(true);
        
        Text manualControlLabel = new Text("Manual Controls");
        manualControlLabel.setStyle("-fx-font-weight: bold;");
        rightSideGrid.getChildren().add(manualControlLabel);
        
        moveFwdBtn = new MoveButton("Forward");
        moveRevBtn = new MoveButton("Reverse");
        moveStopBtn = new MoveButton("Stop");
        moveLeftBtn = new MoveButton("Turn Left");
        moveRightBtn = new MoveButton("Turn Right");
        
        ToggleGroup moveGroup = new ToggleGroup();
        moveFwdBtn.setToggleGroup(moveGroup);
        moveRevBtn.setToggleGroup(moveGroup);
        moveStopBtn.setToggleGroup(moveGroup);
        moveLeftBtn.setToggleGroup(moveGroup);
        moveRightBtn.setToggleGroup(moveGroup);
        moveFwdBtn.setDisable(true);
        moveRevBtn.setDisable(true);
        moveStopBtn.setDisable(true);
        moveLeftBtn.setDisable(true);
        moveRightBtn.setDisable(true);
        
        moveStopBtn.setSelected(true);
        
        HBox manualControlRow1 = new HBox(10);
        manualControlRow1.setAlignment(Pos.CENTER);
        HBox manualControlRow2 = new HBox(10);
        manualControlRow2.setAlignment(Pos.CENTER);
        HBox manualControlRow3 = new HBox(10);
        manualControlRow3.setAlignment(Pos.CENTER);
        
        manualControlRow1.getChildren().add(moveFwdBtn);
        manualControlRow2.getChildren().add(moveLeftBtn);
        manualControlRow2.getChildren().add(moveStopBtn);
        manualControlRow2.getChildren().add(moveRightBtn);
        manualControlRow3.getChildren().add(moveRevBtn);
        
        rightSideGrid.getChildren().add(manualControlRow1);
        rightSideGrid.getChildren().add(manualControlRow2);
        rightSideGrid.getChildren().add(manualControlRow3);
        
        final Separator separator = new Separator();
        separator.setOrientation(Orientation.VERTICAL);
        mainGrid.getChildren().add(separator);
        mainGrid.getChildren().add(rightSideGrid);
    }
    
    // Set the handler to be called when the connect button is clicked
    public void setConnectBtnHandler(EventHandler<ActionEvent> handler)
    {
        connectBtn.setOnAction(handler);
    }
    
    // Set the handler to be called when the disconnect button is clicked
    public void setDisconnectBtnHandler(EventHandler<ActionEvent> handler)
    {
        disconnectBtn.setOnAction(handler);
    }
    
    // Set the handler to be called when the start mapping button is clicked
    public void setStartMappingBtnHandler(EventHandler<ActionEvent> handler)
    {
        startMappingBtn.setOnAction(handler);
    }
    
    // Set the handler to be called when the stop mapping button is clicked
    public void setStopMappingBtnHandler(EventHandler<ActionEvent> handler)
    {
        stopMappingBtn.setOnAction(handler);
    }
    
    // Set the handler to be called when the reset button is clicked
    public void setResetMappingBtnHandler(EventHandler<ActionEvent> handler)
    {
        resetMappingBtn.setOnAction(handler);
    }
    
    // Set the handler to be called when operating mode is set to automated
    public void setAutomatedOperationBtnHandler(EventHandler<ActionEvent> handler)
    {
        operatingModeAutoBtn.setOnAction(handler);
    }
    
    // Set the handler to be called when operating mode is set to manual
    public void setManualOperationBtnHandler(EventHandler<ActionEvent> handler)
    {
        operatingModeManualBtn.setOnAction(handler);
    }
    
    public void setMoveStopBtnHandler(EventHandler<ActionEvent> handler)
    {
        moveStopBtn.setOnAction(handler);
        moveStopBtn.selectedProperty().addListener(new ManualControlChangeListener());
    }
    
    class ManualControlChangeListener implements ChangeListener<Boolean>
    {
        @Override
       public void changed(ObservableValue ov, Boolean prevVal, Boolean newVal)
       {
           // By default, select stop button if nothing else is selected
           if (!anyManualControlsSelected())
                    clickMoveBtn(MoveCommandType.STOP, true);
       }
    }
    
    public void setMoveFwdBtnHandler(EventHandler<ActionEvent> handler)
    {
         moveFwdBtn.setOnAction(handler);
         moveFwdBtn.selectedProperty().addListener(new ManualControlChangeListener());
    }
    
    public void setMoveRevBtnHandler(EventHandler<ActionEvent> handler)
    {
        moveRevBtn.setOnAction(handler);
        moveRevBtn.selectedProperty().addListener(new ManualControlChangeListener());
    }
    
    public void setMoveLeftBtnHandler(EventHandler<ActionEvent> handler)
    {
        moveLeftBtn.setOnAction(handler);
        moveLeftBtn.selectedProperty().addListener(new ManualControlChangeListener());
    }
    
    public void setMoveRightBtnHandler(EventHandler<ActionEvent> handler)
    {
        moveRightBtn.setOnAction(handler);
        moveRightBtn.selectedProperty().addListener(new ManualControlChangeListener());
    }
    
    public void toggleAutomatedControls(boolean enabled)
    {
        startMappingBtn.setDisable(!enabled);
        stopMappingBtn.setDisable(!enabled);
        resetMappingBtn.setDisable(!enabled);
    }
    
    // Simulate a click on one of the manual movement button
    // @param allowUntoggle: allows the simulated click to untoggle the targeted 
    // button (such that the state defaults to "stop" if untoggled)
    public void clickMoveBtn(MoveCommandType direction, boolean allowUntoggle)
    {
        ToggleButton moveBtn = null;
        switch (direction)
        {
            case STOP:
                moveBtn = moveStopBtn;
                break;
            case LEFT:
                moveBtn = moveLeftBtn;
                break;
            case RIGHT:
                moveBtn = moveRightBtn;
                break;
            case REVERSE:
                moveBtn = moveRevBtn;
                break;
            case FORWARD:
                moveBtn = moveFwdBtn;
                break;
        }
        
        if (moveBtn != null && (!moveBtn.isSelected() || allowUntoggle))
            moveBtn.fire();
    }
    
    // Return true if any controls are selected (toggled)
    public boolean anyManualControlsSelected()
    {
        if (moveStopBtn.selectedProperty().get()
            || moveFwdBtn.selectedProperty().get()
            || moveRevBtn.selectedProperty().get()
            || moveLeftBtn.selectedProperty().get()
            || moveRightBtn.selectedProperty().get())
        {
            return true;
        }
        return false;
    }
    
    public void toggleManualControls(boolean enabled)
    {
        moveFwdBtn.setDisable(!enabled);
        moveRevBtn.setDisable(!enabled);
        moveLeftBtn.setDisable(!enabled);
        moveRightBtn.setDisable(!enabled);
        moveStopBtn.setDisable(!enabled);
    }
    
    // Toggles the status (active/disabled) of the connect button
    public void toggleConnectBtn(boolean enabled)
    {
        connectBtn.setDisable(!enabled);
    }
    
    // Toggles the status (active/disabled) of the disconnect button
    public void toggleDisconnectBtn(boolean enabled)
    {
        disconnectBtn.setDisable(!enabled);
    }
    
    // Toggles the status (active/disabled) of the start mapping button
    public void toggleStartMappingBtn(boolean enabled)
    {
        startMappingBtn.setDisable(!enabled);
    }
    
    // Toggles the status (active/disabled) of the stop mapping button
    public void toggleStopMappingBtn(boolean enabled)
    {
        stopMappingBtn.setDisable(!enabled);
    }
    
    // Toggles the status (active/disabled) of the reset mapping button
    public void toggleResetMappingBtn(boolean enabled)
    {
        resetMappingBtn.setDisable(!enabled);
    }
    
    // Set the COM port dropdown box's data source
    public void setAvailableComPortSource(ObservableList<ComPort> comPorts)
    {
        comPortDropdown.setItems(comPorts);
    }
    
    public ComPort getSelectedComPort()
    {
        ComPort comPort = (ComPort) comPortDropdown.getValue();
        return comPort;
    }
    
    public int getGridAreaX()
    {
        return Integer.parseInt(gridSizeX.getText());
    }
    
    public int getGridAreaY()
    {
        return Integer.parseInt(gridSizeY.getText());
    }
    
    // Toggles value and color of the connected status label
    public void toggleConnectedStatus(ConnectionStatus status)
    {
        String statusString = "";
        Color statusColor = Color.BLACK;
        
        switch (status)
        {
            case CONNECTED:
                statusString = CONNECTION_STATUS_TRUE;
                statusColor = CONNECTION_STATUS_TRUE_COLOR;
                break;
            case NOT_CONNECTED:
                statusString = CONNECTION_STATUS_FALSE;
                statusColor = CONNECTION_STATUS_FALSE_COLOR;
                break;
            case ATTEMPTING_CONNECTION:
                statusString = CONNECTION_STATUS_ATTEMPTING;
                statusColor = CONNECTION_STATUS_ATTEMPTING_COLOR;
                break;
            case CONNECTION_FAILED:
                statusString = CONNECTION_STATUS_FAILED;
                statusColor = CONNECTION_STATUS_FAILED_COLOR;
                break;
        }
        
        connectionStatusValue.setText(statusString);
        connectionStatusValue.setFill(statusColor);
    }
    
    // Toggles value and color of the mapping status label
    public void toggleMappingStatus(boolean connected)
    {
        String statusString = connected ? MAPPING_STATUS_TRUE : MAPPING_STATUS_FALSE;
        Color statusColor = connected ? MAPPING_STATUS_TRUE_COLOR : MAPPING_STATUS_FALSE_COLOR;
        mappingStatusValue.setText(statusString);
        mappingStatusValue.setFill(statusColor);
    }
}
