package mappingbotapp.ui;

import javafx.scene.control.TextField;

/*
* This class represents a numeric (digital) text field.
*/
public class NumericTextField extends TextField
{
    @Override
    public void replaceText(int start, int end, String text)
    {
        // Only update the textfield if input is numeric
        if (isNumeric(text))
        {
            super.replaceText(start, end, text);
        }
    }

    @Override
    public void replaceSelection(String text)
    {
        // Only replace the textfield if input is numeric
        if (isNumeric(text))
        {
            super.replaceSelection(text);
        }
    }

    private boolean isNumeric(String text)
    {
        // Check if the input is numeric
        boolean numeric = text.matches("[0-9\b]*");
        return numeric;
    }
}