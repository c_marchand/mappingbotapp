package mappingbotapp.ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Pane;

/*
* The Menu bar view is the toolbar at the top of the program that shows
* additional program options.
*/
public class MenuBarView
{
    private static final String MENU_FILE_LABEL = "File";
    private static final String MENU_EXPORT_DATA_LABEL = "Export Data";
    private static final String MENU_IMPORT_LABEL = "Import Data";
    
    private final MenuBar menuBar;
    private final MenuItem exportMenuItem;
    private final MenuItem importMenuItem;
    
    public MenuBarView(Pane root)
    {
        menuBar = new MenuBar();
 
        // Set up the "File" menu
        Menu menuFile = new Menu(MENU_FILE_LABEL);
        exportMenuItem = new MenuItem(MENU_EXPORT_DATA_LABEL);
        importMenuItem = new MenuItem(MENU_IMPORT_LABEL);    
        menuFile.getItems().addAll(exportMenuItem, importMenuItem);
 
        menuBar.getMenus().addAll(menuFile);
 
        root.getChildren().addAll(menuBar);
    }
    
    // Set the handler for the export button
    public void setMenuExportHandler(EventHandler<ActionEvent> handler)
    {
        exportMenuItem.setOnAction(handler);
    }
    
    // Set the handler for the import button
    public void setMenuImportHandler(EventHandler<ActionEvent> handler)
    {
        importMenuItem.setOnAction(handler);
    }
}
