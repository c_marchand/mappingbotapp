package mappingbotapp.ui;

import javafx.scene.control.ToggleButton;

/*
* The MoveButton class is used for the movement buttons in the control panel.
*/
public class MoveButton extends ToggleButton
{
    public MoveButton(String label)
    {
        // Same as ToggleButton, but with fixed width
        super(label);
        this.setPrefWidth(80);
    }
}
