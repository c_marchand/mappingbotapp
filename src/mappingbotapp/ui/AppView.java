package mappingbotapp.ui;

import java.io.File;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.FileChooser;

import mappingbotapp.model.IDataStoreModel;

/*
* This class sets up the view. Only presentation is done here, minimal logic.
*/
public class AppView
{
    private static final String WINDOW_TITLE = "Autonomous Mapping Boat Operator Software";
    
    private static final int VIEW_WIDTH = 1007;
    private static final int VIEW_HEIGHT = 460;
    
    private final Stage primaryStage; // Just for file selection @temp
    private final Scene scene;

    private final ControlPanelView controlPanel;
    private final DataPanelView dataPanel;
    private final MenuBarView menuBarView;
    
    public AppView(Stage primaryStage_, IDataStoreModel dataStore)
    {    
        primaryStage = primaryStage_;
        
        primaryStage.setResizable(false);
        primaryStage.setTitle(WINDOW_TITLE);
        
        // Initializes root scene node
        VBox root = new VBox();

        // Initializes the main (vertical) grid
        HBox verticalGrid = new HBox(0);
        
        // Initialize subpanels
        menuBarView = new MenuBarView(root);
        controlPanel = new ControlPanelView(verticalGrid);
        dataPanel = new DataPanelView(verticalGrid, dataStore);
        
        scene = new Scene(root, VIEW_WIDTH, VIEW_HEIGHT);
        primaryStage.setScene(scene);
        scene.getStylesheets().add(AppView.class.getResource("View.css").toExternalForm());
        
        root.getChildren().addAll(verticalGrid);
        
        primaryStage.show();
    }
    
    public ControlPanelView getControlPanel()
    {
        return controlPanel;
    }
    
    public MenuBarView getMenuBarView()
    { 
        return menuBarView;
    }
    
    // Show the user a dialog to get a destination file
    // @param save : true if saving file, false if opening file
    public File getSelectedFile(boolean save)
    {
        FileChooser fileChooser = new FileChooser();
  
        // Set extension filter (CSV files)
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show the save file or open file dialog dialog
        File file = null;
        if (save)
            file = fileChooser.showSaveDialog(primaryStage);
        else
            file = fileChooser.showOpenDialog(primaryStage);
        
        return file;
    }
    
    // Show an alert dialog of specified type with specified message contents
    public void showAlert(Alert.AlertType type, String message)
    {
        Alert alert = new Alert(type);
        
        // Generate title based on alert type
        String alertTitle = "";
        switch (type)
        {
            case INFORMATION:
                alertTitle = "Information";
                break;
            case ERROR:
                alertTitle = "Error";
                break;
            case WARNING:
                alertTitle = "Warning";
                break;
            default:
                alertTitle = "Information";
        }
        
        alert.setTitle(alertTitle);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
    
    public Scene getScene()
    {
        return scene;
    }
}
