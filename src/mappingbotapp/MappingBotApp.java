package mappingbotapp;

import javafx.application.Application;
import javafx.stage.Stage;

import mappingbotapp.model.DataStoreModel;
import mappingbotapp.model.DummyCommunicationModel;
import mappingbotapp.model.ICommunicationModel;
import mappingbotapp.model.DummyControlModel;
import mappingbotapp.model.IControlModel;
import mappingbotapp.model.IDataStoreModel;
import mappingbotapp.model.CommunicationModel;
import mappingbotapp.model.ControlModel;
import mappingbotapp.model.GpsControlModel;
import mappingbotapp.ui.AppController;
import mappingbotapp.ui.AppView;

/*
* This class is in charge of launching the application.
*/
public class MappingBotApp extends Application
{
    @Override
    public void start(Stage primaryStage)
    {  
        // Instantiate the model classes (using mock classes for now)
        // Swap with the real classes when available
        IDataStoreModel dataStore = new DataStoreModel();
        IControlModel controlModel = new ControlModel(dataStore);
        
        ICommunicationModel commsModel = new DummyCommunicationModel(controlModel);
        controlModel.setDataStore(dataStore);
        controlModel.setCommunicationModel(commsModel);
        
        // Create the view (presentation) and the controller (user interaction)
        AppView view = new AppView(primaryStage, dataStore);
        AppController controller = new AppController(dataStore, view, controlModel,
                commsModel);
        
        // Testing for motor command values - will leave this here
        // in case it's needed for testing
        /*
        MotorCommand testStateOffFwd = new MotorCommand(MotorCommand.Speed.OFF, MotorCommand.Direction.FORWARD);
        MotorCommand testStateOffRev = new MotorCommand(MotorCommand.Speed.OFF, MotorCommand.Direction.REVERSE);
        MotorCommand testStateLowFwd = new MotorCommand(MotorCommand.Speed.LOW_SPEED, MotorCommand.Direction.FORWARD);
        MotorCommand testStateMedFwd = new MotorCommand(MotorCommand.Speed.MED_SPEED, MotorCommand.Direction.FORWARD);
        MotorCommand testStateHighFwd = new MotorCommand(MotorCommand.Speed.HIGH_SPEED, MotorCommand.Direction.FORWARD);
        MotorCommand testStateLowRev = new MotorCommand(MotorCommand.Speed.LOW_SPEED, MotorCommand.Direction.REVERSE);
        MotorCommand testStateMedRev = new MotorCommand(MotorCommand.Speed.MED_SPEED, MotorCommand.Direction.REVERSE);
        MotorCommand testStateHighRev = new MotorCommand(MotorCommand.Speed.HIGH_SPEED, MotorCommand.Direction.REVERSE);
        
        byte result = testStateOffFwd.toByte();
        System.out.println(String.format("%02X ", result));
        result = testStateOffRev.toByte();
        System.out.println(String.format("%02X ", result));
        result = testStateLowFwd.toByte();
        System.out.println(String.format("%02X ", result));
        result = testStateMedFwd.toByte();
        System.out.println(String.format("%02X ", result));
        result = testStateHighFwd.toByte();
        System.out.println(String.format("%02X ", result));
        result = testStateLowRev.toByte();
        System.out.println(String.format("%02X ", result));
        result = testStateMedRev.toByte();
        System.out.println(String.format("%02X ", result));
        result = testStateHighRev.toByte();
        System.out.println(String.format("%02X ", result));
        */
        
        // Testing packet decoding
        /*byte[] testBytes = {0x00, 0x03, 0x00, 0x00, 0x01, 0x01, 0x00 };
        BotPacket packet = PacketDecoder.decode(testBytes);
        System.out.println("Status: " + packet.getStatusCode());
        System.out.println("BotLocationData depth: " + packet.getLocationData().depth);*/
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
